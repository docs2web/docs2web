from docs2web.common import BoundingBox


def test_within():
    bbox1 = BoundingBox(67.5, 364.495, 290.65985, 355.995)
    bbox2 = BoundingBox(67.5, 353.9975, 90.4585, 345.4975)

    assert bbox1.within(bbox2, 5)
