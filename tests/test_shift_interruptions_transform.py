from docs2web.common import *
from docs2web.transform import shift_interruptions

def test_simple_case():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 2")
                ]
            )
        )
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 3
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 2"

def test_non_interruption():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 2")
                ]
            )
        )
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 3
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 2"

def test_text_only_single_header():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 2")
                ]
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 3")
                ]
            )
        )
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 4
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 3"
    assert shifted_blocks[3].block.segments[0].text == "Paragraph 2"

def test_interruption_at_end():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 2")
                ]
            ),
            tag="interruption"
        )
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 3
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 2"

def test_text_only_double_header():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 2")
                ]
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 3.")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 4.")
                ]
            )
        ),
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 2")
                ]
            )
        ),
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 6
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 3."
    assert shifted_blocks[3].block.segments[0].text == "Paragraph 4."
    assert shifted_blocks[4].block.segments[0].text == "Paragraph 2"
    assert shifted_blocks[5].block.segments[0].text == "Header 2"

def test_recursive_interruption():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 2")
                ]
            ),
            tag="interruption1"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 3")
                ]
            ),
            tag="interruption2"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 4")
                ]
            ),
            tag="interruption1"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 5.")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 6.")
                ]
            )
        ),
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 2")
                ]
            )
        ),
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 8
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 5."
    assert shifted_blocks[3].block.segments[0].text == "Paragraph 6."
    assert shifted_blocks[4].block.segments[0].text == "Paragraph 2"
    assert shifted_blocks[5].block.segments[0].text == "Paragraph 4"
    assert shifted_blocks[6].block.segments[0].text == "Paragraph 3"
    assert shifted_blocks[7].block.segments[0].text == "Header 2"


def test_image_single_header():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            IMAGE,
            ExtractedImage(
                "test.bmp"
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 3.")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 4.")
                ]
            )
        )
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 5
    assert shifted_blocks[0].style == HEADER_1
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].style == PARAGRAPH
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].style == PARAGRAPH
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 3."
    assert shifted_blocks[3].style == IMAGE
    assert shifted_blocks[3].block.file == "test.bmp"
    assert shifted_blocks[4].style == PARAGRAPH
    assert shifted_blocks[4].block.segments[0].text == "Paragraph 4."


def test_image_single_header():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            IMAGE,
            ExtractedImage(
                "test.bmp"
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 3.")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 4.")
                ]
            )
        )
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 5
    assert shifted_blocks[0].style == HEADER_1
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].style == PARAGRAPH
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].style == PARAGRAPH
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 3."
    assert shifted_blocks[3].style == PARAGRAPH
    assert shifted_blocks[3].block.segments[0].text == "Paragraph 4."
    assert shifted_blocks[4].style == IMAGE
    assert shifted_blocks[4].block.file == "test.bmp"


def test_complex():
    orig_blocks = [
        ExtractedBlock(
            HEADER_1,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 1")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 1")
                ]
            )
        ),
        ExtractedBlock(
            HEADER_2,
            ExtractedText(
                [
                    ExtractedTextSegment("Header 2")
                ]
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 2.")
                ]
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            IMAGE,
            ExtractedImage(
                "test.bmp"
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 3.")
                ]
            ),
            tag="interruption"
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 4.")
                ]
            )
        ),
        ExtractedBlock(
            PARAGRAPH,
            ExtractedText(
                [
                    ExtractedTextSegment("Paragraph 5.")
                ]
            )
        )
    ]

    shifted_blocks = shift_interruptions(orig_blocks)
    assert len(shifted_blocks) == 8
    assert shifted_blocks[0].style == HEADER_1
    assert shifted_blocks[0].block.segments[0].text == "Header 1"
    assert shifted_blocks[1].style == PARAGRAPH
    assert shifted_blocks[1].block.segments[0].text == "Paragraph 1"
    assert shifted_blocks[2].style == PARAGRAPH
    assert shifted_blocks[2].block.segments[0].text == "Paragraph 4."
    assert shifted_blocks[3].style == PARAGRAPH
    assert shifted_blocks[3].block.segments[0].text == "Paragraph 5."
    assert shifted_blocks[4].style == HEADER_2
    assert shifted_blocks[4].block.segments[0].text == "Header 2"
    assert shifted_blocks[5].style == PARAGRAPH
    assert shifted_blocks[5].block.segments[0].text == "Paragraph 2."
    assert shifted_blocks[6].style == IMAGE
    assert shifted_blocks[6].block.file == "test.bmp"
    assert shifted_blocks[7].style == PARAGRAPH
    assert shifted_blocks[7].block.segments[0].text == "Paragraph 3."
