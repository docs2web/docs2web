from setuptools import setup, find_packages

setup(
    name="docs2web",
    version="0.1.0",
    description="Transforms various documents to web friend formats.",
    url='',
    author='',
    packages=find_packages(),
    install_requires=[
        "click",
        "camelot-py",
        "Wand",
        "python-docx",
        "pdfminer.six",
        "odfpy"
    ]
)
