import typing
import re

from . import common

ENDING_PUNC = [".", "?", ":", "!"]


def apply_replacements(
    replacement_config: typing.Dict[str, typing.Any], raw_document: common.RawDocument
) -> common.RawDocument:
    text_replacements = replacement_config.get("text", [])
    if text_replacements:
        for page in raw_document.pages:
            for section in page.sections:
                for block in section.blocks:
                    if block.block_type == common.RAW_TEXT_BLOCK:
                        for segmentIdx, segment in enumerate(block.block.segments):
                            for replacement in text_replacements:
                                find = replacement.get("find")
                                if not find or find != segment.text.strip():
                                    continue

                                replace = replacement.get("replace")
                                if not replace:
                                    continue

                                font_name = replacement.get(
                                    "font_name", segment.font_name
                                )
                                font_size = replacement.get(
                                    "font_size", segment.font_size
                                )
                                if (
                                    font_name != segment.font_name
                                    or font_size != segment.font_size
                                ):
                                    continue

                                segment = common.RawTextSegment(
                                    segment.text.replace(find, replace),
                                    segment.font_name,
                                    segment.font_size,
                                    segment.bold,
                                    segment.italic,
                                    segment.underline,
                                    segment.strikethrough,
                                    segment.stroke_colour,
                                    segment.fill_colour,
                                    segment.link,
                                )
                                block.block.segments[segmentIdx] = segment

    return raw_document


def apply_pre_extract_transforms(
    extract_conf: common.DocumentConfiguration, raw_document: common.RawDocument
) -> common.RawDocument:

    replacement_config = extract_conf.transforms.get("replacements", None)
    if replacement_config:
        raw_document = apply_replacements(replacement_config, raw_document)

    return raw_document


def shift_interruptions(
    extracted_blocks: typing.List[common.ExtractedBlock],
) -> typing.List[common.ExtractedBlock]:
    new_blocks = []
    interrupted_style = None
    interrupted_tag = None
    interrupters: typing.List[common.ExtractedBlock] = []

    for extracted_block in extracted_blocks:
        if not interrupted_style:
            if extracted_block.style == common.PARAGRAPH:
                all_text = extracted_block.block.join().strip()
                if all_text and all_text[-1] not in ENDING_PUNC:
                    interrupted_style = extracted_block.style
                    interrupted_tag = extracted_block.tag
        else:
            if extracted_block.tag == interrupted_tag or not extracted_block.tag:
                if extracted_block.style in common.HEADER_LEVEL_LOOKUP:
                    if interrupters:
                        new_blocks += shift_interruptions(interrupters)
                        interrupters = []
                    interrupted_style = None
                    interrupted_tag = None
            else:
                interrupters.append(extracted_block)
                continue

        new_blocks.append(extracted_block)

    if interrupters:
        new_blocks += shift_interruptions(interrupters)

    return new_blocks


def collapse_text_segments(
    extracted_blocks: typing.List[common.ExtractedBlock],
) -> typing.List[common.ExtractedBlock]:
    collapsed_blocks = []
    skip = 0

    for extracted in extracted_blocks:
        if not extracted.style in [common.TABLE, common.IMAGE]:
            to_remove = []
            for segment_idx, segment in enumerate(extracted.block.segments):
                if not segment.text.strip():
                    to_remove.append(segment_idx)

            for segment_idx in to_remove[::-1]:
                del extracted.block.segments[segment_idx]

            if not extracted.block.join().strip():
                continue

            extracted = common.ExtractedBlock(
                extracted.style,
                common.ExtractedText(
                    segment.mutate(
                        text=common.MULTIPLE_WHITESPACE_RE.sub(" ", segment.text)
                    )
                    for segment in extracted.block.segments
                ),
                extracted.page_number,
                extracted.source,
                extracted.tag,
            )
        collapsed_blocks.append(extracted)

    extracted_blocks = collapsed_blocks
    collapsed_blocks = []

    for extracted_idx, extracted in enumerate(extracted_blocks):
        if skip > 0:
            skip -= 1
            continue

        if not extracted.style in [common.TABLE, common.IMAGE]:
            extracted_text: common.ExtractedText = extracted.block
            text: str = ""
            last_segment: typing.Optional[common.ExtractedTextSegment] = None
            old_segments: typing.List[common.ExtractedTextSegment] = []
            old_segments += extracted_text.segments
            next_idx = extracted_idx + 1
            sources = [extracted.source]

            if extracted.style not in [common.UNSORTED_LIST, common.ORDERED_LIST]:
                while (
                    (
                        old_segments[-1].text.rstrip()[-1] not in ENDING_PUNC
                        or extracted.style != common.PARAGRAPH
                    )
                    and next_idx < len(extracted_blocks)
                    and extracted_blocks[next_idx].style == extracted.style
                ):
                    old_segments += extracted_blocks[next_idx].block.segments
                    sources.append(extracted_blocks[next_idx].source)
                    next_idx += 1
                    skip += 1

            new_segments = []
            for segment in old_segments:
                if last_segment is None:
                    text = segment.text
                elif (
                    last_segment.bold == segment.bold
                    and last_segment.italic == segment.italic
                    and last_segment.underline == segment.underline
                    and last_segment.strikethrough == segment.strikethrough
                ):
                    if text.strip().endswith("-"):
                        text = text.strip()[:-1] + segment.text
                    elif text:
                        text += segment.text
                    else:
                        text = segment.text
                else:
                    new_segments.append(last_segment.mutate(text=text))
                    text = segment.text

                last_segment = segment

            if text:
                new_segments.append(last_segment.mutate(text=text))

            if new_segments:
                extracted = common.ExtractedBlock(
                    style=extracted.style,
                    block=common.ExtractedText(new_segments),
                    page_number=extracted.page_number,
                    source="\n".join(sources),
                    tag=extracted.tag,
                )
            else:
                extracted = None

        if extracted:
            collapsed_blocks.append(extracted)

    return collapsed_blocks


def extract_unsorted_lists(
    extracted_blocks: typing.List[common.ExtractedBlock], bullet_char: str = "•"
) -> typing.List[common.ExtractedBlock]:
    new_blocks = []
    for extracted_block in extracted_blocks:
        if extracted_block.style not in [common.PARAGRAPH, common.BLOCK_QUOTE]:
            new_blocks.append(extracted_block)
            continue

        paragraph_segments: typing.List[common.ExtractedTextSegment] = []
        bullet_segments: typing.List[typing.List[common.ExtractedTextSegment]] = []
        current_bullet: typing.List[common.ExtractedTextSegment] = []

        for segment in extracted_block.block.segments:
            if bullet_char in segment.text or current_bullet or bullet_segments:
                starts_with_bullet = segment.text.lstrip().startswith(bullet_char)
                if starts_with_bullet:
                    segment_text = segment.text.lstrip()
                else:
                    segment_text = segment.text
                first = True

                for text in segment_text.split(bullet_char):
                    if current_bullet and (not first or starts_with_bullet):
                        bullet_segments.append(current_bullet)
                        current_bullet = []
                    first = False

                    if (
                        not starts_with_bullet
                        and not bullet_segments
                        and not current_bullet
                    ):
                        paragraph_segments.append(segment.mutate(text=text))
                    else:
                        current_bullet.append(segment.mutate(text=text))
            else:
                paragraph_segments.append(segment)

        if current_bullet:
            bullet_segments.append(current_bullet)

        if not bullet_segments:
            new_blocks.append(extracted_block)
        else:
            if extracted_block.style == common.BLOCK_QUOTE:
                if paragraph_segments:
                    segments = [
                        segment
                        for segment in paragraph_segments
                        if segment.text.strip()
                    ]
                    if segments:
                        new_blocks.append(
                            common.ExtractedBlock(
                                style=common.BLOCK_QUOTE,
                                block=common.ExtractedText(segments=segments),
                                page_number=extracted_block.page_number,
                                source=extracted_block.source,
                                tag=extracted_block.tag,
                            )
                        )
                for bullet_segment_list in bullet_segments:
                    segments = [
                        segment
                        for segment in bullet_segment_list
                        if segment.text.strip()
                    ]
                    if segments:
                        segments.insert(0, common.ExtractedTextSegment(bullet_char))
                        new_blocks.append(
                            common.ExtractedBlock(
                                style=common.BLOCK_QUOTE,
                                block=common.ExtractedText(segments=segments),
                                page_number=extracted_block.page_number,
                                source=extracted_block.source,
                                tag=extracted_block.tag,
                            )
                        )
            else:
                if paragraph_segments:
                    segments = [
                        segment
                        for segment in paragraph_segments
                        if segment.text.strip()
                    ]
                    if segments:
                        new_blocks.append(
                            common.ExtractedBlock(
                                style=common.PARAGRAPH,
                                block=common.ExtractedText(segments=segments),
                                page_number=extracted_block.page_number,
                                source=extracted_block.source,
                                tag=extracted_block.tag,
                            )
                        )
                for bullet_segment_list in bullet_segments:
                    segments = [
                        segment
                        for segment in bullet_segment_list
                        if segment.text.strip()
                    ]
                    if segments:
                        new_blocks.append(
                            common.ExtractedBlock(
                                style=common.UNSORTED_LIST,
                                block=common.ExtractedText(segments=segments),
                                page_number=extracted_block.page_number,
                                source=extracted_block.source,
                                tag=extracted_block.tag,
                            )
                        )

    return new_blocks


def apply_errata(
    errata: typing.List[common.Errata],
    extracted_blocks: typing.List[common.ExtractedBlock],
) -> typing.Tuple[
    typing.List[common.ExtractedBlock],
    typing.List[typing.List[common.ErrataApplication]],
]:
    if not errata:
        return extracted_blocks

    errata_results: typing.List[typing.List[common.ErrataApplication]] = []
    for _ in errata:
        errata_results.append([])

    last_headings = ["", "", "", "", ""]
    new_blocks = []
    for extracted_block in extracted_blocks:
        if extracted_block.page_number == 0:
            new_blocks.append(extracted_block)
            continue  # cannot apply errata on pageless
        elif extracted_block.style == common.IMAGE:
            new_blocks.append(extracted_block)
            continue
        elif extracted_block.style == common.TABLE:
            new_blocks.append(extracted_block)
            continue  # not supporting errata in tables currently
        elif extracted_block.style == common.HEADER_1:
            last_headings[0] = common.NOT_WORDS_RE.sub(
                "", extracted_block.block.join().lower()
            )
            last_headings[1:5] = ["", "", "", ""]
        elif extracted_block.style == common.HEADER_2:
            last_headings[1] = common.NOT_WORDS_RE.sub(
                "", extracted_block.block.join().lower()
            )
            last_headings[2:5] = ["", "", ""]
        elif extracted_block.style == common.HEADER_3:
            last_headings[2] = common.NOT_WORDS_RE.sub(
                "", extracted_block.block.join().lower()
            )
            last_headings[3:5] = ["", ""]
        elif extracted_block.style == common.HEADER_4:
            last_headings[3] = common.NOT_WORDS_RE.sub(
                "", extracted_block.block.join().lower()
            )
            last_headings[4] = ""
        elif extracted_block.style == common.HEADER_5:
            last_headings[4] = common.NOT_WORDS_RE.sub(
                "", extracted_block.block.join().lower()
            )

        for errata_idx, errata_item in enumerate(errata):
            if errata_item.disable:
                continue
            if extracted_block.page_number in errata_item.pages:
                heading_match = not errata_item.headings
                if not heading_match:
                    for heading in errata_item.headings:
                        if heading in last_headings:
                            heading_match = True
                            break

                if heading_match:
                    if errata_item.replace:
                        for segment_idx, text_segment in enumerate(
                            extracted_block.block.segments
                        ):
                            if errata_item.is_regex:
                                changed_text = re.sub(
                                    errata_item.find,
                                    errata_item.replace,
                                    text_segment.text,
                                    flags=re.IGNORECASE,
                                )
                            else:
                                changed_text = re.sub(
                                    re.escape(errata_item.find),
                                    errata_item.replace,
                                    text_segment.text,
                                    flags=re.IGNORECASE,
                                )

                            if changed_text != text_segment.text:
                                errata_results[errata_idx].append(
                                    common.ErrataApplication(
                                        errata[errata_idx],
                                        extracted_block,
                                        last_headings,
                                    )
                                )

                                segment_dict = text_segment._asdict()
                                segment_dict[common.RAW_TEXT_BLOCK] = changed_text

                                extracted_block.block.segments[
                                    segment_idx
                                ] = common.ExtractedTextSegment(**segment_dict)
                    elif (
                        errata_item.append_paragraph
                        or errata_item.prepend_paragraph
                        or errata_item.append_ordered_list
                        or errata_item.prepend_ordered_list
                        or errata_item.append_unsorted_list
                        or errata_item.prepend_unsorted_list
                    ):

                        if (
                            errata_item.append_unsorted_list
                            or errata_item.prepend_unsorted_list
                        ):
                            new_block_style = common.PARAGRAPH
                            new_block_content = (
                                errata_item.append_unsorted_list
                                or errata_item.prepend_unsorted_list
                            )

                        elif (
                            errata_item.append_ordered_list
                            or errata_item.prepend_ordered_list
                        ):
                            new_block_style = common.ORDERED_LIST
                            new_block_content = (
                                errata_item.append_ordered_list
                                or errata_item.prepend_ordered_list
                            )

                        else:
                            new_block_style = common.PARAGRAPH
                            new_block_content = (
                                errata_item.append_paragraph
                                or errata_item.prepend_paragraph
                            )

                        if isinstance(new_block_content, str):
                            new_block = common.ExtractedText(
                                [common.ExtractedTextSegment(new_block_content)]
                            )

                        if isinstance(new_block_content, dict):
                            new_block_content = [new_block_content]

                        if isinstance(new_block_content, list):
                            new_block = common.ExtractedText(
                                [
                                    common.ExtractedTextSegment(**item)
                                    for item in new_block_content
                                ]
                            )

                        matched = False
                        if errata_item.is_regex:
                            matched = bool(
                                re.match(
                                    errata_item.find,
                                    extracted_block.block.join(),
                                    flags=re.IGNORECASE,
                                )
                            )
                        else:
                            matched = bool(
                                errata_item.find.lower()
                                in extracted_block.block.join().lower()
                            )

                        if matched:
                            errata_results[errata_idx].append(
                                common.ErrataApplication(
                                    errata[errata_idx],
                                    extracted_block,
                                    last_headings,
                                )
                            )

                            if (
                                errata_item.append_paragraph
                                or errata_item.append_ordered_list
                                or errata_item.append_unsorted_list
                            ):
                                new_blocks.append(extracted_block)
                                extracted_block = common.ExtractedBlock(
                                    new_block_style,
                                    new_block,
                                    page_number=extracted_block.page_number,
                                    source=errata_item.source,
                                    tag=extracted_block.tag,
                                )
                            else:
                                new_blocks.append(
                                    common.ExtractedBlock(
                                        new_block_style,
                                        new_block,
                                        page_number=extracted_block.page_number,
                                        source=errata_item.source,
                                        tag=extracted_block.tag,
                                    )
                                )

        new_blocks.append(extracted_block)

    return new_blocks, errata_results


def collapse_headers(
    extracted_blocks: typing.List[common.ExtractedBlock],
) -> typing.List[common.ExtractedBlock]:
    new_blocks: typing.List[common.ExtractedBlock] = []

    extracted_block: common.ExtractedBlock
    last_block: common.ExtractedBlock = None
    for extracted_block in reversed(extracted_blocks):
        if last_block is None:
            new_blocks.append(extracted_block)
            last_block = extracted_block
            continue

        last_block_header = common.HEADER_LEVEL_LOOKUP.get(last_block.style)
        if last_block_header is None:
            new_blocks.append(extracted_block)
            last_block = extracted_block
            continue

        current_block_header = common.HEADER_LEVEL_LOOKUP.get(extracted_block.style)
        if current_block_header is None:
            new_blocks.append(extracted_block)
            last_block = extracted_block
            continue

        if current_block_header > last_block_header:
            extracted_block = common.ExtractedBlock(
                last_block.style,
                extracted_block.block,
                extracted_block.page_number,
                extracted_block.source,
                extracted_block.tag,
            )

        new_blocks.append(extracted_block)
        last_block = extracted_block

    new_blocks.reverse()
    return new_blocks


def apply_autolinks(extracted_doc: common.ExtractedDocument):
    for extracted_block in extracted_doc.entries:
        if extracted_block.style != common.PARAGRAPH:
            continue
        for segment_idx, segment in enumerate(extracted_block.block.segments):
            if not (segment.bold or segment.italic or segment.underline):
                continue
            text_index = []
            text = common.get_header_key(segment.text, text_index)
            for header_key, header_list in extracted_doc.heading_lookup.items():
                if header_key == text:
                    for header in header_list:
                        if header.level > 3:
                            continue
                        header_heirarchy = header.get_heirarchy()

                        link = common.ExtractedLink(
                            internal_headers=header_heirarchy,
                            internal_page=header_list[0].block.page_number,
                        )

                        extracted_block.block.segments[segment_idx] = segment.mutate(
                            link=link
                        )
                        break


def apply_post_extract_transforms(
    extract_conf: common.DocumentConfiguration,
    extracted_blocks: typing.List[common.ExtractedBlock],
    skip_errata: bool = False,
) -> common.ExtractedDocument:
    extracted_blocks = shift_interruptions(extracted_blocks)
    extracted_blocks = collapse_headers(extracted_blocks)
    extracted_blocks = collapse_text_segments(extracted_blocks)
    extracted_blocks = extract_unsorted_lists(extracted_blocks)

    # Ensure the first block is always a HEADER 1, create one if it does not exist
    if not extracted_blocks or extracted_blocks[0].style != common.HEADER_1:
        synthetic_block = common.ExtractedBlock(
            style=common.HEADER_1,
            block=common.ExtractedText(
                [common.ExtractedTextSegment(extract_conf.title)]
            ),
            page_number=extracted_blocks[0].page_number if extracted_blocks else 0,
            source="Generated by ExtractedDocument",
            tag=extracted_blocks[0].tag if extracted_blocks else "",
        )
        extracted_blocks.insert(0, synthetic_block)

    applied = []
    unapplied = []
    if extract_conf.errata:
        if not skip_errata:
            errata_results = []
            print("Applying errata")
            extracted_blocks, errata_results = apply_errata(
                extract_conf.errata, extracted_blocks
            )
            applied = [application for application in errata_results if application]
            disabled_count = len(
                [errata for errata in extract_conf.errata if errata.disable]
            )
            print(
                f"Applied {len(applied)} of {len(extract_conf.errata)} errata ({disabled_count} are disabled)"
            )

            unapplied = [
                extract_conf.errata[idx]
                for idx, application in enumerate(errata_results)
                if not application and not extract_conf.errata[idx].disable
            ]
            print(
                f"Could not apply {len(unapplied)} of {len(extract_conf.errata)} errata:"
            )
            for errata_item in unapplied:
                print(f"  Errata {errata_item}`")

        else:
            print("Skipping errata")
    extracted_doc = common.ExtractedDocument(
        extracted_blocks, extract_conf.errata, applied, unapplied
    )

    apply_autolinks(extracted_doc)

    return extracted_doc
