from __future__ import annotations
import typing
import re
import pathlib
import logging

from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.layout import (
    LTChar,
    LAParams,
    LTTextContainer,
    LTText,
    LTAnno,
    LTPage,
    TextLineElement,
)
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.utils import bbox2str

try:
    import camelot.core
except ImportError:
    pass
from wand.image import Image

from .. import common, cache

# Apparently speeds up pdfminer.six: https://stackoverflow.com/a/67028710
pdflogs = [
    logging.getLogger(name)
    for name in logging.root.manager.loggerDict
    if name.startswith("pdfminer")
]
for ll in pdflogs:
    ll.setLevel(logging.WARNING)


class TagContainer(LTTextContainer[TextLineElement]):
    def __init__(self, tag, props):
        super().__init__()
        self.tag = tag
        self.props = props

    def __repr__(self):
        return "<%s %s tag=%s props=%s %r>" % (
            self.__class__.__name__,
            bbox2str(self.bbox),
            self.tag,
            self.props,
            self.get_text(),
        )


class PageAggregatorWithTags(PDFPageAggregator):
    def begin_tag(self, tag, props=None):
        self._stack.append(self.cur_item)
        self.cur_item = TagContainer(tag, props)
        return

    def end_tag(self):
        tag = self.cur_item
        assert isinstance(self.cur_item, TagContainer), str(type(self.cur_item))
        self.cur_item = self._stack.pop()
        self.cur_item.add(tag)
        return

    def do_tag(self, tag, props=None):
        self.begin_tag(tag, props)
        self.end_tag()
        return


class FontConfig(typing.NamedTuple):
    bold: bool = False
    italic: bool = False


class TableHandler:
    def __init__(
        self,
        pages: common.PageMatch,
        only_tags: typing.List[str],
        except_tags: typing.List[str],
        header_required: bool,
        header_fonts: typing.List[common.FontMatch],
        cell_fonts: typing.List[common.FontMatch],
    ) -> None:
        self.pages = pages
        self.only_tags = only_tags
        self.except_tags = except_tags
        self.header_fonts = header_fonts
        self.cell_fonts = cell_fonts
        self.header_required = header_required

    def matches(
        self,
        page_number: int,
        segment: common.RawTextSegment,
        first_element: bool,
        section_tag: str,
    ) -> bool:
        if page_number not in self.pages:
            return False

        if self.only_tags and section_tag not in self.only_tags:
            return False

        if self.except_tags and section_tag in self.except_tags:
            return False

        if not self.header_fonts and not self.cell_fonts:
            return False

        for font_match in self.header_fonts:
            if font_match.matches(segment):
                return True

        if not self.header_required or not first_element:
            for font_match in self.cell_fonts:
                if font_match.matches(segment):
                    return True

        return False

    @staticmethod
    def from_config(config: typing.Dict[str, typing.Any]) -> TableHandler:
        config["pages"] = common.PageMatch(config.get("pages"))
        config.setdefault("header_required", True)
        config.setdefault("only_tags", [])
        config.setdefault("except_tags", [])

        header_fonts = []
        for match_config in config.get("header_fonts", []):
            match_config["colours"] = [
                common.Colour(**colour)
                if isinstance(colour, dict)
                else common.Colour(*colour)
                for colour in match_config.get("colours", [])
            ]
            header_fonts.append(common.FontMatch(**match_config))
        config["header_fonts"] = header_fonts

        cell_fonts = []
        for match_config in config.get("cell_fonts", []):
            match_config["colours"] = [
                common.Colour(**colour)
                if isinstance(colour, dict)
                else common.Colour(*colour)
                for colour in match_config.get("colours", [])
            ]
            cell_fonts.append(common.FontMatch(**match_config))
        config["cell_fonts"] = cell_fonts

        mode = config.pop("mode", "image")
        if mode == "image":
            return ImageTableHandler(**config)

        raise ValueError(f"Unrecognized table mode {mode}")

    def create_table(
        self,
        pdf_file: str,
        page_number: int,
        page_width: int,
        page_height: int,
        segments: typing.List[common.RawTextSegment],
        tag: str = "",
    ) -> typing.Optional[common.RawBlock]:
        raise NotImplementedError()


class ImageTableHandler(TableHandler):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def create_table(
        self,
        pdf_file: str,
        page_number: int,
        page_width: int,
        page_height: int,
        segments: typing.List[common.RawTextSegment],
        tag: str = "",
    ) -> typing.Optional[common.RawBlock]:
        bbox = common.BoundingBox.from_list(
            (segment.bbox for segment in segments), top_to_bottom=False
        )

        return common.RawBlock(
            block_type=common.RAW_IMAGE_BLOCK,
            block=common.RawImage(
                file=pathlib.Path(
                    f"table_p{page_number}_{bbox.left}_{bbox.top}_{bbox.right}_{bbox.bottom}.jpg"
                ),
                callback=get_image_extract_callback(
                    pdf_file,
                    page_number,
                    page_width,
                    page_height,
                    bbox.left,
                    bbox.top,
                    bbox.right,
                    bbox.bottom,
                ),
                description=f"Extracted table from page {page_number}",
            ),
            tag=tag,
        )


class FontRegistry:
    EMPTY_FONT_CONFIG = FontConfig()

    def __init__(self, font_mapping: typing.Dict[str, typing.Any]):
        self._font_configs = [
            (re.compile(font_regex), FontConfig(**font_config))
            for font_regex, font_config in font_mapping.items()
        ]

    def get(self, raw_font_name: str) -> typing.Tuple[str, FontConfig]:
        for font_re, font_config in self._font_configs:
            match = font_re.fullmatch(raw_font_name)
            if match:
                groups = match.groups()
                if groups:
                    font_name = groups[0]
                else:
                    font_name = raw_font_name

                return font_name, font_config
        return raw_font_name, FontRegistry.EMPTY_FONT_CONFIG


def visualise_page(
    pdf_file: str,
    page_number: int,
    page_width: int,
    page_height: int,
    *,
    left: typing.Optional[int] = None,
    right: typing.Optional[int] = None,
    top: typing.Optional[int] = None,
    bottom: typing.Optional[int] = None,
    border: typing.Optional[int] = None,
) -> bytes:
    left = left or 0
    bottom = bottom or 0
    right = right or page_width
    top = top or page_height

    def load_image(item_id: str, item: typing.Optional[pathlib.Path]) -> Image:
        return Image(filename=item_id, resolution=150)

    def close_image(item_id: str, item: typing.Optional[pathlib.Path], img: Image):
        img.close()

    img: Image = cache.get_cache_item(
        f"{pdf_file}[{page_number - 1}]",
        pathlib.Path(pdf_file),
        load_image,
        close_image,
    )

    img.antialias = False

    ratio = img.width / page_width

    border = border or 0
    left = int(ratio * max(left - border, 0))
    right = int(ratio * min(right + border, page_width))
    bottom = int(ratio * (page_height - max(bottom - border, 0)))
    top = int(ratio * (page_height - min(top + border, page_height)))

    with img.clone() as to_crop:
        to_crop.crop(left=left, top=top, right=right, bottom=bottom)
        return to_crop.make_blob("jpeg")


def get_stroke_colour(char: LTChar):
    if char.graphicstate:
        if char.graphicstate.scolor and len(char.graphicstate.scolor) == 3:
            return common.Colour(
                *[int(comp * 256) for comp in char.graphicstate.scolor]
            )
    return common.Colour()


def get_nonstroke_colour(char: LTChar):
    if char.graphicstate:
        if char.graphicstate.ncolor and len(char.graphicstate.ncolor) == 3:
            return common.Colour(
                *[int(comp * 256) for comp in char.graphicstate.ncolor]
            )
    return common.Colour()


def get_image_extract_callback(
    pdf_file, page_number, page_width, page_height, left, top, right, bottom
):
    def get_image():
        return visualise_page(
            pdf_file,
            page_number,
            page_width=page_width,
            page_height=page_height,
            left=left,
            top=top,
            right=right,
            bottom=bottom,
            border=2,
        )

    return get_image


def parse_text_container(
    font_registry: FontRegistry, text_container: LTTextContainer
) -> typing.Generator[common.RawTextSegment, None, None]:
    if not hasattr(text_container, "_objs"):
        return []

    first = next(iter(text_container))
    if not isinstance(first, (LTChar, LTAnno)):
        for child in text_container:
            yield from parse_text_container(font_registry, child)
        return

    first = True
    char: LTText
    text = ""
    top = text_container.y1
    bottom = text_container.y0

    for char in text_container:
        if isinstance(char, LTChar):
            if first:
                first = False

                font_name, font_config = font_registry.get(char.fontname)
                font_size = char.size
                text_scolour = get_stroke_colour(char)
                text_ncolour = get_nonstroke_colour(char)

                left = char.x0
                right = char.x1
            else:
                new_font_name, new_font_config = font_registry.get(char.fontname)
                new_font_size = char.size
                new_text_scolour = get_stroke_colour(char)
                new_text_ncolour = get_nonstroke_colour(char)

                if (
                    new_font_name != font_name
                    or (abs(font_size - new_font_size) > 0.1)
                    or (new_text_scolour != text_scolour)
                    or (new_text_ncolour != text_ncolour)
                    or (new_font_config.bold != font_config.bold)
                    or (new_font_config.italic != font_config.italic)
                ):
                    yield common.RawTextSegment(
                        text,
                        font_name,
                        font_size,
                        bold=font_config.bold,
                        italic=font_config.italic,
                        stroke_colour=text_scolour,
                        fill_colour=text_ncolour,
                        bbox=common.BoundingBox(left, top, right, bottom),
                    )
                    text = ""

                    font_name = new_font_name
                    font_config = new_font_config
                    font_size = new_font_size
                    text_scolour = new_text_scolour
                    text_ncolour = new_text_ncolour

                    left = char.x0
                    right = char.x1
                else:
                    left = min(left, char.x0)
                    right = max(right, char.x1)
        text += char.get_text()

    if text:
        yield common.RawTextSegment(
            text,
            font_name,
            font_size,
            bold=font_config.bold,
            italic=font_config.italic,
            stroke_colour=text_scolour,
            fill_colour=text_ncolour,
            bbox=common.BoundingBox(left, top, right, bottom),
            paragraph_break=True,
        )


def parse_pdf_elements(
    pdf_file: str,
    page_number: int,
    page_width: int,
    page_height: int,
    table_handlers: typing.List[TableHandler],
    segments: typing.List[common.RawTextSegment],
    layout_def: common.LayoutDefinition,
    layout_section: common.LayoutSection,
    max_line_spacing: float,
) -> typing.Generator[common.RawBlock, None, None]:
    current_block: typing.List[common.RawTextSegment] = []
    current_bbox = None

    table_handler = None
    table_segments = []

    section_tag = "" if not layout_section else layout_section.tag

    if layout_section and layout_section.handling == common.HANDLING_FIGURE:
        figure_left = 9999
        figure_right = 0
        figure_top = 0
        figure_bottom = 9999

        for figure_element in segments:
            figure_left = min(figure_element.bbox.left, figure_left)
            figure_right = max(figure_element.bbox.right, figure_right)
            figure_top = max(figure_element.bbox.top, figure_top)
            figure_bottom = min(figure_element.bbox.bottom, figure_bottom)

        yield common.RawBlock(
            block_type=common.RAW_IMAGE_BLOCK,
            block=common.RawImage(
                file=pathlib.Path(
                    f"figure_p{page_number}_{figure_left}_{figure_top}_{figure_right}_{figure_bottom}.jpg"
                ),
                callback=get_image_extract_callback(
                    pdf_file,
                    page_number,
                    page_width,
                    page_height,
                    figure_left,
                    figure_top,
                    figure_right,
                    figure_bottom,
                ),
                description=f"Extracted figure from page {page_number}",
            ),
            tag=section_tag,
        )
        return

    def build_text():
        notes = ""
        if layout_def:
            notes = f"Layout: {layout_def.notes}"
        return common.RawBlock(
            common.RAW_TEXT_BLOCK,
            common.RawTextBlock(current_block),
            current_bbox,
            notes,
            tag=section_tag,
        )

    for segment in segments:
        if table_handler is None:
            for handler in table_handlers:
                if handler.matches(page_number, segment, True, section_tag):
                    table_handler = handler
                    if current_block:
                        yield build_text()
                        current_block = []
                    break

        if table_handler:
            if table_handler.matches(page_number, segment, False, section_tag):
                table_segments.append(segment)
            else:
                table_block = table_handler.create_table(
                    pdf_file,
                    page_number,
                    page_width,
                    page_height,
                    table_segments,
                    tag=section_tag,
                )
                if table_block is not None:
                    yield table_block
                table_handler = None
                table_segments = []

        if not table_handler:
            can_join = True
            if current_block and (
                (current_block[-1].font_name != segment.font_name)
                or (abs(current_block[-1].font_size - segment.font_size) > 0.1)
                or not segment.bbox.within(current_bbox, max_line_spacing)
            ):
                can_join = False

            if can_join:
                current_block.append(segment)
                if segment.paragraph_break:
                    current_bbox = common.BoundingBox.from_list(
                        (seg.bbox for seg in current_block), top_to_bottom=False
                    )
                    yield build_text()
                    current_block = []
            else:
                yield build_text()
                current_block = [segment]

            if current_block:
                current_bbox = common.BoundingBox.from_list(
                    (seg.bbox for seg in current_block), top_to_bottom=False
                )

    if current_block:
        yield build_text()

    if table_handler:
        table_block = table_handler.create_table(
            pdf_file,
            page_number,
            page_width,
            page_height,
            table_segments,
            tag=section_tag,
        )
        if table_block is not None:
            yield table_block


def recurse_pdf_element(
    font_registry: FontRegistry, root_element, seen: typing.Set[typing.Any] = None
) -> typing.Generator[common.RawTextSegment, None, None]:
    if seen is None:
        seen = set()
    for child in root_element:
        if child in seen:
            continue
        seen.add(child)

        if isinstance(child, LTTextContainer):
            yield from parse_text_container(font_registry, child)
        elif hasattr(child, "__iter__"):
            yield from recurse_pdf_element(font_registry, child, seen)


def extract_raw_document(
    extract_conf: common.DocumentConfiguration,
    *,
    process_dropped: bool = False,
    **kwargs,
) -> common.RawDocument:
    font_registry = FontRegistry(extract_conf.input_config.get("font_mapping", {}))

    layouts = common.build_layouts(extract_conf.input_config.get("layouts", []))
    table_handlers = [
        TableHandler.from_config(handler_config)
        for handler_config in extract_conf.input_config.get("table_handling", [])
    ]
    with_tags = extract_conf.input_config.get("process_tags", False)

    max_line_spacing: float = extract_conf.input_config.get("max_line_spacing", 5.0)

    def do_load(
        item_id: str, pdf_path: typing.Optional[pathlib.Path]
    ) -> typing.List[LTPage]:
        laparams = extract_conf.input_config.get("laparams", {}).copy()
        laparams.update({"all_texts": True, "boxes_flow": None})

        pdf_pages: typing.List[LTPage] = []
        with open(pdf_path, "rb") as fp:
            parser = PDFParser(fp)
            doc = PDFDocument(parser)
            resource_manager = PDFResourceManager()

            if with_tags:
                device = PageAggregatorWithTags(
                    resource_manager, laparams=LAParams(**laparams)
                )
            else:
                device = PDFPageAggregator(
                    resource_manager, laparams=LAParams(**laparams)
                )

            interpreter = PDFPageInterpreter(resource_manager, device)

            page: PDFPage
            for page in PDFPage.create_pages(doc):
                interpreter.process_page(page)
                pdf_pages.append(device.get_result())

            return pdf_pages

    pdf_pages: typing.List[LTPage] = cache.get_cache_item(
        f"pdfminer:{extract_conf.file}", extract_conf.file, do_load, can_evict=False
    )

    mapped_layouts: typing.Dict[int, common.LayoutDefinition] = {}
    for layout_def in layouts:
        layout_def.pages.apply_mappings(mapped_layouts, layout_def, len(pdf_pages))

    pages = []
    for page_number, page in enumerate(pdf_pages, 1):
        layout_def = mapped_layouts.get(page_number)
        raw_sections: typing.List[common.RawSection] = []
        if layout_def:
            element: common.RawTextSegment

            dropped_segments: typing.List[common.RawTextSegment] = []
            sections: typing.List[typing.List[common.RawTextSegment]] = []
            for _ in range(len(layout_def.sections) + 1):
                sections.append([])

            for element in recurse_pdf_element(font_registry, page):
                if (
                    (layout_def.x_bounds[0] > element.bbox.left)
                    or (layout_def.x_bounds[1] < element.bbox.right)
                    or (layout_def.y_bounds[0] > element.bbox.bottom)
                    or (layout_def.y_bounds[1] < element.bbox.top)
                ):
                    if process_dropped:
                        dropped_segments.append(element)
                    continue

                for section_idx, section in enumerate(layout_def.sections):
                    if section.includes_bbox(element.bbox):
                        if section.handling != common.HANDLING_DROP:
                            sections[section_idx].append(element)
                        elif process_dropped:
                            dropped_segments.append(element)
                        break
                else:
                    sections[-1].append(element)

            for section, layout_section in zip(
                sections, layout_def.sections + [common.DEFAULT_LAYOUT_SECTION]
            ):
                section.sort(
                    key=lambda element: (-element.bbox.top, element.bbox.bottom)
                )
                raw_blocks: typing.List[common.RawBlock] = list(
                    parse_pdf_elements(
                        str(extract_conf.file),
                        page_number,
                        page.width,
                        page.height,
                        table_handlers,
                        section,
                        layout_def,
                        layout_section,
                        max_line_spacing,
                    )
                )
                raw_sections.append(common.RawSection(raw_blocks, layout_section))
        elif process_dropped:
            dropped_segments = list(recurse_pdf_element(font_registry, page))

        if process_dropped:
            dropped_segments.sort(
                key=lambda element: (-element.bbox.top, element.bbox.left)
            )
            dropped_blocks = parse_pdf_elements(
                str(extract_conf.file),
                page_number,
                page.width,
                page.height,
                [],
                dropped_segments,
                None,
                None,
                max_line_spacing,
            )
        else:
            dropped_blocks = []

        pages.append(
            common.RawPage(
                page_number=page_number,
                width=page.width,
                height=page.height,
                sections=raw_sections,
                dropped=dropped_blocks,
            )
        )

    return common.RawDocument(pages=pages, top_to_bottom_coords=False)
