import typing

import odf
import odf.element
import odf.opendocument

from .. import common


class FontConfig(typing.NamedTuple):
    font_name: str
    bold: bool = False


class OdtStyle(typing.NamedTuple):
    font: typing.Optional[FontConfig] = None
    font_size: typing.Optional[str] = None
    parent_style_name: typing.Optional[str] = None
    bold: bool = False
    italic: bool = False
    underline: bool = False
    strikethrough: bool = False
    stroke_colour: common.Colour = common.Colour()
    fill_colour: common.Colour = common.Colour()
    master_page_name: typing.Optional[str] = None
    break_before: bool = False
    break_after: bool = False


default_odt_style = OdtStyle()


def parse_odt_row(
    styles: typing.Dict[str, OdtStyle],
    row_node: odf.element.Element,
    rows: typing.List[common.RawTableRow],
    header: bool = False,
):
    child_node: odf.element.Element
    cells = []
    for child_node in row_node.childNodes:
        if child_node.qname == (
            "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
            "table-cell",
        ):
            raw_pages: typing.List[common.RawPage] = [
                common.RawPage(1, [common.RawSection([])])
            ]
            parse_odt_node(styles, raw_pages, child_node, [], {})

            blocks = [
                block
                for page in raw_pages
                for section in page.sections
                for block in section.blocks
            ]
            cells.append(common.RawTableCell(blocks))
    if cells:
        rows.append(common.RawTableRow(cells))


def parse_odt_table(
    styles: typing.Dict[str, OdtStyle],
    blocks: typing.List[common.RawBlock],
    table_node: odf.element.Element,
):
    child_node: odf.element.Element
    rows: typing.List[common.RawTableRow] = []
    for child_node in table_node.childNodes:
        if child_node.qname == (
            "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
            "table-header-rows",
        ):
            for header_row in child_node.childNodes:
                if header_row.qname == (
                    "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
                    "table-row",
                ):
                    parse_odt_row(styles, header_row, rows, header=True)
        elif child_node.qname == (
            "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
            "table-row",
        ):
            parse_odt_row(styles, child_node, rows)

    if rows:
        blocks.append(
            common.RawBlock(
                common.RAW_TABLE_BLOCK,
                common.RawTable(max(len(row.cells) for row in rows), len(rows), rows),
            )
        )


def get_style_font_name_and_size(
    style: typing.Optional[OdtStyle],
) -> typing.Tuple[typing.Optional[str], typing.Optional[int]]:
    if not style or not style.font:
        return None, None

    font_size = None
    if style.font_size:
        if style.font_size.endswith("pt") and style.font_size[:-2].isnumeric():
            font_size = int(style.font_size[:-2])

    return style.font.font_name, font_size


def parse_odt_node(
    styles: typing.Dict[str, OdtStyle],
    pages: typing.List[common.RawPage],
    node: odf.element.Element,
    style_names: typing.List[str],
    state: typing.Dict[str, typing.Any],
    indent: int = 0,
) -> typing.Union[common.RawTextSegment, common.RawBlock]:
    if node.nodeType == odf.element.Element.TEXT_NODE:
        style = None
        font_name = None
        font_size = None

        for style_name in reversed(style_names):
            if not style:
                style = styles.get(style_name)

            style_font_name, style_font_size = get_style_font_name_and_size(
                styles.get(style_name)
            )
            font_name = font_name or style_font_name
            font_size = font_size or style_font_size

        style = style or default_odt_style
        font_name = font_name or "Unnamed"
        font_size = font_size or 11

        return [
            common.RawTextSegment(
                node.data,
                font_name,
                font_size,
                style.bold,
                style.italic,
                style.underline,
                style.strikethrough,
                style.stroke_colour,
                style.fill_colour,
            )
        ]
    elif node.nodeType == odf.element.Element.ELEMENT_NODE:
        # print(("  " * indent), node.tagName, node.qname)
        if node.qname == ("urn:oasis:names:tc:opendocument:xmlns:table:1.0", "table"):
            parse_odt_table(styles, pages[-1].sections[-1].blocks, node)
            return []
        if node.qname == (
            "urn:oasis:names:tc:opendocument:xmlns:text:1.0",
            "soft-page-break",
        ):
            pages.append(
                common.RawPage(pages[-1].page_number + 1, [common.RawSection([])])
            )
            return []

        if node.qname == ("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "section"):
            pages[-1].sections.append(common.RawSection([]))

        child_node: odf.element.Element

        style_name = node.attributes.get(
            ("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "style-name")
        )
        break_before = False
        break_after = False
        if style_name:
            style_names.append(style_name)
            style = styles.get(style_name)
            while style is not None:
                break_before = break_before or style.break_before
                break_after = break_after or style.break_after

                style = styles.get(style.parent_style_name)

        if break_before:
            pages.append(
                common.RawPage(pages[-1].page_number + 1, [common.RawSection([])])
            )

        segments = []
        for child_node in node.childNodes:
            new_segments = parse_odt_node(
                styles, pages, child_node, style_names, state, indent=indent + 1
            )
            if new_segments:
                segments += new_segments

        if style_name:
            style_names.pop()

        if segments and node.qname == (
            "urn:oasis:names:tc:opendocument:xmlns:text:1.0",
            "p",
        ):
            pages[-1].sections[-1].blocks.append(
                common.RawBlock(common.RAW_TEXT_BLOCK, common.RawTextBlock(segments))
            )
            segments = []

        if break_after:
            pages.append(
                common.RawPage(pages[-1].page_number + 1, [common.RawSection([])])
            )

        return segments


def collapse_style(
    styles: typing.Dict[str, OdtStyle], style_name: str
) -> typing.Dict[str, typing.Any]:
    style = styles.get(style_name)
    if style == None:
        return {}

    if style.parent_style_name is not None:
        collapsed = collapse_style(styles, style.parent_style_name)
        collapsed.update(
            {key: value for key, value in style._asdict().items() if value is not None}
        )
        return collapsed

    return style._asdict()


def extract_raw_document(
    extract_conf: common.DocumentConfiguration,
    **kwargs,
) -> common.RawDocument:

    doc = odf.opendocument.load(str(extract_conf.file))

    fonts: typing.Dict[str, FontConfig] = {}
    font_face_node: odf.element.Element
    for font_face_node in doc.fontfacedecls.childNodes:
        key = font_face_node.attributes[
            ("urn:oasis:names:tc:opendocument:xmlns:style:1.0", "name")
        ]
        fonts[key] = FontConfig(
            font_name=font_face_node.attributes.get(
                (
                    "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0",
                    "font-family",
                ),
                key,
            ),
            bold="bold"
            in font_face_node.attributes.get(
                ("urn:oasis:names:tc:opendocument:xmlns:style:1.0", "font-adornments"),
                "",
            ).lower(),
        )

    styles: typing.Dict[str, OdtStyle] = {}
    style_node: odf.element.Element
    for style_node in doc.styles.childNodes + doc.automaticstyles.childNodes:
        style_name = style_node.attributes.get(
            ("urn:oasis:names:tc:opendocument:xmlns:style:1.0", "name")
        )
        if not style_name:
            continue

        style_attrs = {
            "parent_style_name": style_node.attributes.get(
                (
                    "urn:oasis:names:tc:opendocument:xmlns:style:1.0",
                    "parent-style-name",
                ),
            ),
            "master_page_name": style_node.attributes.get(
                ("urn:oasis:names:tc:opendocument:xmlns:style:1.0", "master-page-name")
            ),
        }
        child_node: odf.element.Element
        for child_node in style_node.childNodes:
            for attr, attr_value in child_node.attributes.items():
                if attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:style:1.0",
                    "font-name",
                ):
                    style_attrs["font"] = fonts.get(attr_value)
                elif attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
                    "font-size",
                ):
                    style_attrs["font_size"] = attr_value
                elif attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
                    "font-weight",
                ):
                    if attr_value != "normal":
                        style_attrs["bold"] = True
                    else:
                        style_attrs["bold"] = False
                elif attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
                    "font-style",
                ):
                    if attr_value != "normal":
                        style_attrs["italic"] = True
                    else:
                        style_attrs["italic"] = False
                elif attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
                    "text-underline-mode",
                ):
                    style_attrs["underline"] = True
                elif attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
                    "text-line-through-mode",
                ):
                    style_attrs["strikethrough"] = True
                elif attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
                    "break-before",
                ):
                    style_attrs["break_before"] = True
                elif attr == (
                    "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0",
                    "break-after",
                ):
                    style_attrs["break_after"] = True

        styles[style_name] = OdtStyle(**style_attrs)

    final_styles = {
        style_name: OdtStyle(**collapse_style(styles, style_name))
        for style_name in styles
    }

    raw_pages: typing.List[common.RawPage] = [
        common.RawPage(1, [common.RawSection([])])
    ]
    parse_odt_node(final_styles, raw_pages, doc.body, [], {})

    return common.RawDocument(raw_pages)
