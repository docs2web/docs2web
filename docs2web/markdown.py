import pathlib
import re
import typing

from black import err

from . import common


class MarkdownStyle:
    def clean_filename(self, name: str) -> str:
        if len(name) > 50:
            name = name[:50]
        name = name.replace("]", "").replace("[", "")
        return re.sub("[^a-z0-9]", "_", name.lower()).strip("_")

    def get_appendix_filename(
        self, source_directory: pathlib.Path, appendix_type: str
    ) -> typing.Tuple[pathlib.Path, str]:
        return source_directory, appendix_type

    def get_filename(
        self,
        source_directory: pathlib.Path,
        headers: typing.List[common.ExtractedHeading],
    ) -> typing.Tuple[pathlib.Path, str]:
        header = headers[-1]
        title = self.clean_filename(header.title)
        filename = f"p{header.block.page_number}_{title}"
        return source_directory, filename

    def get_rendered_name(self, output_filename: pathlib.Path) -> typing.Optional[str]:
        return None

    def get_header(
        self,
        headers: typing.Union[typing.List[common.ExtractedHeading], str],
        is_appendix: bool = False,
    ) -> str:
        return ""

    def get_footer(self, is_appendix: bool = False) -> str:
        return ""

    def resolve_link(
        self, from_file: pathlib.Path, link: common.ExtractedLink
    ) -> typing.Optional[str]:
        if link.external_link:
            return link.external_link
        if link.internal_headers:
            for header in reversed(link.internal_headers):
                if header.rendered_filename:
                    return (
                        "../" * (len(from_file.parents) - 1)
                    ) + header.rendered_filename.replace("\\", "/")
            print(
                f"No rendered filename for {[header.block.block.join() for header in link.internal_headers]}"
            )
        return None

    def open_bold(self) -> str:
        return ""

    def close_bold(self) -> str:
        return ""

    def open_italic(self) -> str:
        return ""

    def close_italic(self) -> str:
        return ""

    def open_underline(self) -> str:
        return ""

    def close_underline(self) -> str:
        return ""

    def open_strikethrough(self) -> str:
        return ""

    def close_strikethrough(self) -> str:
        return ""

    def make_link(self, text, dest) -> str:
        return f"[{text}]({dest})"


class CommonMarkStyle(MarkdownStyle):
    def open_bold(self) -> str:
        return "**"

    def close_bold(self) -> str:
        return "**"

    def open_italic(self) -> str:
        return "_"

    def close_italic(self) -> str:
        return "_"

    def make_link(self, text, dest) -> str:
        return f"[{text}]({dest})"


def join_with_styling(
    filename: pathlib.Path,
    markdown_style: MarkdownStyle,
    extracted_text: common.ExtractedText,
):
    text = ""

    style_stack = []

    for segment in extracted_text.segments:
        if not segment.text.strip():
            continue

        segment_text = segment.text
        segment_text = (
            re.sub("[\r\n\t ]+", " ", segment_text)
            .replace("<", "&lt;")
            .replace(">", "&gt;")
            .replace("[", "&#91;")
            .replace("]", "&#93;")
        )

        # Check closing
        styles_to_close = set()
        if "b" in style_stack and not segment.bold:
            styles_to_close.add("b")
        if "i" in style_stack and not segment.italic:
            styles_to_close.add("i")
        if "u" in style_stack and not segment.underline:
            styles_to_close.add("u")
        if "s" in style_stack and not segment.strikethrough:
            styles_to_close.add("s")

        was_style_closed = False
        while styles_to_close and style_stack:
            style = style_stack.pop()
            if style == "b":
                text = text.strip() + markdown_style.close_bold()
            elif style == "i":
                text = text.strip() + markdown_style.close_italic()
            elif style == "u":
                text = text.strip() + markdown_style.close_underline()
            elif style == "s":
                text = text.strip() + markdown_style.close_strikethrough()
            if style in styles_to_close:
                styles_to_close.remove(style)
                was_style_closed = True

        if text and was_style_closed:
            text += " "

        # Check Opening
        was_style_opened = False
        if "s" not in style_stack and segment.strikethrough:
            if text and not text.endswith(" ") and not was_style_opened:
                text += " "
            was_style_opened = True
            text += markdown_style.open_strikethrough()
            if text and not text.endswith(" "):
                text += " "
            style_stack.append("s")
        if "u" not in style_stack and segment.underline:
            if text and not text.endswith(" ") and not was_style_opened:
                text += " "
            was_style_opened = True
            text += markdown_style.open_underline()
            style_stack.append("u")
        if "i" not in style_stack and segment.italic:
            if text and not text.endswith(" ") and not was_style_opened:
                text += " "
            was_style_opened = True
            text += markdown_style.open_italic()
            style_stack.append("i")
        if "b" not in style_stack and segment.bold:
            if text and not text.endswith(" ") and not was_style_opened:
                text += " "
            was_style_opened = True
            text += markdown_style.open_bold()
            style_stack.append("b")

        if segment.link:
            link_dest = markdown_style.resolve_link(filename, segment.link)
            if link_dest:
                text += markdown_style.make_link(segment_text, link_dest)
            else:
                text += segment_text
        else:
            text += segment_text

    text = text.strip()

    # Close any remaining tags
    style_stack.reverse()
    for style in style_stack:
        if style == "b":
            text += markdown_style.close_bold()
        elif style == "i":
            text += markdown_style.close_italic()
        elif style == "u":
            text += markdown_style.close_underline()
        elif style == "s":
            text += markdown_style.close_strikethrough()
    return text.strip()


def output_markdown(
    extract_conf: common.DocumentConfiguration,
    markdown_style: MarkdownStyle,
    extracted_document: common.ExtractedDocument,
    markdown_directory: pathlib.Path,
    image_directory: typing.Optional[pathlib.Path] = None,
    image_url_base: str = "./",
    *,
    output_debug: bool = False,
    file_header_level: int = 1,
):
    def header_clean(line_text):
        return common.propercase(line_text.replace("\n", " ").replace("\r", ""))

    def list_item_clean(line_text, prefix):
        return f"{prefix} " + line_text.replace("\n", " ").replace("\r", "")

    def quote_clean(line_text):
        return "> " + line_text.replace("\n", "\n> ").replace("\r", "")

    def paragraph_clean(line_text):
        return line_text.replace("\n", "").replace("\r", "")

    markdown_directory = markdown_directory.resolve()

    markdown_directory.mkdir(parents=True, exist_ok=True)
    if not image_directory:
        image_directory = markdown_directory
    else:
        image_directory = image_directory.resolve()
        image_directory.mkdir(parents=True, exist_ok=True)

    out_fp = None

    last_style = None

    file_names = set()

    stray_table_warning_issued = set()
    headers: typing.List[typing.Optional[common.ExtractedHeading]] = [None] * 5

    header_filenames: typing.List[
        typing.Tuple[
            typing.List[typing.Optional[common.ExtractedHeading]], pathlib.Path
        ]
    ] = []

    for extracted_heading in extracted_document.headings:
        header_level = common.HEADER_LEVEL_LOOKUP.get(extracted_heading.block.style)
        if header_level > file_header_level:
            continue

        headers = extracted_heading.get_heirarchy()

        directory, base_filename = markdown_style.get_filename(
            markdown_directory, headers
        )

        idx = 1
        filename = directory / f"{base_filename}.md"
        while filename in file_names:
            filename = directory / f"{base_filename}_{idx}.md"
            idx += 1
        file_names.add(filename)

        if extracted_heading.output_filename is None:
            output_path = directory.relative_to(markdown_directory) / filename.name
            extracted_heading.output_filename = str(output_path)

            if extracted_heading.rendered_filename is None:
                rendered_path = markdown_style.get_rendered_name(output_path)
                if rendered_path is not None:
                    extracted_heading.rendered_filename = str(rendered_path)

        header_filenames.append((headers, filename))

    for headers, filename in header_filenames:
        extracted_heading = headers[-1]
        blocks = extracted_heading.get_blocks(file_header_level)

        with common.HashedTextFile(filename) as out_fp:
            out_fp.write(markdown_style.get_header(headers))

            for extracted_block in blocks:
                style = extracted_block.style

                new_lines = "\n\n"
                if style in [common.TABLE, common.IMAGE]:
                    text = ""
                elif style in [
                    common.HEADER_1,
                    common.HEADER_2,
                    common.HEADER_3,
                    common.HEADER_4,
                    common.HEADER_5,
                ]:
                    text = extracted_block.block.join()
                else:
                    text = join_with_styling(
                        filename.relative_to(markdown_directory),
                        markdown_style,
                        extracted_block.block,
                    )

                if style.startswith("H") or style == common.TABLE_HEADER:
                    text = header_clean(text)
                text = common.MULTIPLE_WHITESPACE_RE.sub(" ", text.strip())

                header_level = common.HEADER_LEVEL_LOOKUP.get(style, 0) - (
                    extracted_heading.level - 1
                )

                if header_level == 1:
                    text = "# " + text
                elif header_level == 2:
                    text = "## " + text
                elif header_level == 3:
                    text = "### " + text
                elif header_level == 4:
                    text = "#### " + text
                elif header_level == 5:
                    text = "##### " + text
                elif style == common.BLOCK_QUOTE:
                    text = quote_clean(text)
                    new_lines = "\n"
                elif style == common.UNSORTED_LIST:
                    text = list_item_clean(text, "-")
                    new_lines = "\n"
                elif style == common.TABLE:
                    header_row = True
                    table: common.ExtractedTable = extracted_block.block
                    for row in table.rows:
                        text += "|"
                        for cell in row.cells:
                            for cell_block in cell.contents:
                                if cell_block.style not in [
                                    common.TABLE,
                                    common.IMAGE,
                                    common.TABLE_CELL,
                                    common.TABLE_HEADER,
                                ]:
                                    text += " " + cell_block.block.join().replace(
                                        "\n", "<br />"
                                    ).replace("\r", "")
                            text += " |"

                        text += "\n"
                        if header_row:
                            header_row = False
                            text += "|"
                            for _ in row.cells:
                                text += "----|"
                            text += "\n"
                elif style == common.IMAGE:
                    image: common.ExtractedImage = extracted_block.block

                    dest_file = image_directory / image.file.name
                    if not dest_file.exists():
                        print(f"Writing image: {image.file.name}")
                        image_data = image.read_data()
                        with dest_file.open("wb") as im_fp:
                            im_fp.write(image_data)

                    text = f"![{image.description or 'Image'}]({image_url_base}{dest_file.name})"

                elif style == common.PARAGRAPH:
                    text = paragraph_clean(text)
                elif style in [common.TABLE_HEADER, common.TABLE_CELL]:
                    if extracted_block.page_number not in stray_table_warning_issued:
                        stray_table_warning_issued.add(extracted_block.page_number)
                        print(
                            f"Found stray table elements, they will be treated as paragraphs on page {extracted_block.page_number}"
                        )
                    style = common.PARAGRAPH
                    text = paragraph_clean(text)
                else:
                    print(
                        f"Unexpected style: {style} on page {extracted_block.page_number}"
                    )

                if last_style is not None:
                    if last_style != style:
                        if last_style in [
                            common.UNSORTED_LIST,
                            common.ORDERED_LIST,
                            common.BLOCK_QUOTE,
                        ]:
                            out_fp.write("\n")
                    else:
                        if last_style in [common.BLOCK_QUOTE]:
                            out_fp.write(">\n")

                out_fp.write(text + new_lines)
                if output_debug:
                    out_fp.write(
                        f"```\n{extracted_block.page_number} {extracted_block.style} {extracted_block.source}\n```\n\n"
                    )
                last_style = style

            out_fp.write(markdown_style.get_footer())

    if extracted_document.applied_errata:
        appendix_dir, errata_filename = markdown_style.get_appendix_filename(
            markdown_directory, "AppliedErrata"
        )
        with common.HashedTextFile(appendix_dir / f"{errata_filename}.md") as out_fp:
            out_fp.write(markdown_style.get_header("Applied Errata", is_appendix=True))
            out_fp.write("# Applied Errata\n\n")
            for errata_result in extracted_document.applied_errata:
                errata = errata_result[0].errata
                out_fp.write(f"* **Errata:** {errata}\n")
                if errata.notes:
                    out_fp.write(f"  * Notes: {errata.notes}\n")
                out_fp.write(
                    f"  * Pages: {', '.join(str(application.block.page_number) for application in errata_result)}"
                )
                if errata.count > 1 or len(errata_result) > 1:
                    out_fp.write(
                        f" (Applied {len(errata_result)} of {errata.count} expected)"
                    )
                out_fp.write("\n")

                if errata.source:
                    if errata.source.startswith("http"):
                        out_fp.write(f"  * [Errata Source]({errata.source})\n")
                    else:
                        out_fp.write(f"  * {errata.source}\n")
            out_fp.write(markdown_style.get_footer(is_appendix=True))
