from __future__ import annotations
import typing
import re
import json
import pathlib
import io
import hashlib
import copy

BLOCK_QUOTE = "Q"
HEADER_1 = "H1"
HEADER_2 = "H2"
HEADER_3 = "H3"
HEADER_4 = "H4"
HEADER_5 = "H5"
PARAGRAPH = "P"
TABLE_HEADER = "TH"
TABLE_CELL = "TC"
TABLE = "T"
IMAGE = "I"
SKIP = "S"
UNSORTED_LIST = "UL"
ORDERED_LIST = "OL"

RAW_TEXT_BLOCK = "text"
RAW_TABLE_BLOCK = "table"
RAW_IMAGE_BLOCK = "image"

HANDLING_DROP = "drop"
HANDLING_FIGURE = "figure"

STYLE_NAME_LOOKUP = {
    "quote": BLOCK_QUOTE,
    "header1": HEADER_1,
    "header2": HEADER_2,
    "header3": HEADER_3,
    "header4": HEADER_4,
    "header5": HEADER_5,
    "paragraph": PARAGRAPH,
    "skip": SKIP,
    "unsorted_list": UNSORTED_LIST,
    "ordered_list": ORDERED_LIST,
}

HEADER_LEVEL_LOOKUP = {
    HEADER_1: 1,
    HEADER_2: 2,
    HEADER_3: 3,
    HEADER_4: 4,
    HEADER_5: 5,
}
MULTIPLE_WHITESPACE_RE = re.compile(r"\s+")
PROPER_CASE_RE = re.compile(r"([\w])([\w'’]*)")
NOT_WORDS_RE = re.compile(r"\W")

# Based on https://stackoverflow.com/a/8714242
def make_hash(obj: typing.Any):
    if isinstance(obj, (set, tuple, list)):
        return hash(tuple([make_hash(e) for e in obj]))
    elif isinstance(obj, dict):
        return hash(frozenset((key, make_hash(value)) for key, value in obj.items()))
    return hash(obj)


class PageMatch:
    def __init__(
        self,
        page_defs: typing.Union[
            PageMatch, str, int, typing.List[typing.Union[str, int]]
        ],
        max_pages: int = 9999,
    ):
        if isinstance(page_defs, PageMatch):
            self.pages = page_defs.pages.copy()
            self.all_even = page_defs.all_even
            self.all_odd = page_defs.all_odd
        else:
            pages = set()
            self.all_even = False
            self.all_odd = False

            if page_defs is None:
                page_defs = ["*"]
            elif isinstance(page_defs, (int, str)):
                page_defs = [page_defs]

            for page_def in page_defs:
                if isinstance(page_def, int):
                    pages.add(page_def)
                    continue

                odd = True
                even = True
                negate = False

                if page_def.startswith("!"):
                    negate = True
                    page_def = page_def[1:]
                if page_def.startswith("odd:"):
                    even = False
                    page_def = page_def[4:]
                elif page_def.startswith("even:"):
                    odd = False
                    page_def = page_def[5:]

                if page_def == "*":
                    if even:
                        self.all_even = not negate
                    if odd:
                        self.all_odd = not negate
                else:
                    pos = page_def.find("-")
                    if pos > 0:
                        min_page = int(page_def[:pos])
                        max_page = int(page_def[pos + 1 :])
                    else:
                        min_page = max_page = int(page_def)

                    for page_number in range(min_page, max_page + 1):
                        if not odd and page_number % 2 == 1:
                            continue
                        if not even and page_number % 2 == 0:
                            continue
                        if negate:
                            pages.remove(page_number)
                        else:
                            pages.add(page_number)

            self.pages = pages

    def __contains__(self, page_number: int):
        return self.matches(page_number)

    def matches(self, page_number: int):
        if self.all_even and (page_number % 2) == 0:
            return True
        if self.all_odd and (page_number % 2) == 1:
            return True
        return page_number in self.pages

    def apply_mappings(
        self, mapping: typing.Dict[int, typing.Any], item: typing.Any, page_count: int
    ):
        if self.all_even:
            for page_number in range(2, page_count + 1, 2):
                mapping.setdefault(page_number, item)

        if self.all_odd:
            for page_number in range(1, page_count + 1, 2):
                mapping.setdefault(page_number, item)

        for page_number in self.pages:
            mapping.setdefault(page_number, item)

    def __str__(self):
        return ", ".join(str(page) for page in sorted(self.pages))


DEFAULT_PAGE_MATCH = PageMatch("*")


class FontMatch(typing.NamedTuple):
    name: typing.Optional[str] = None
    min_font_size: typing.Optional[float] = None
    max_font_size: typing.Optional[float] = None
    colours: typing.List[Colour] = []
    bold: typing.Optional[bool] = None
    italic: typing.Optional[bool] = None
    underline: typing.Optional[bool] = None
    strikethrough: typing.Optional[bool] = None

    def matches(self, raw_segment: RawTextSegment):
        if self.name is not None and raw_segment.font_name != self.name:
            return False
        if (
            self.min_font_size is not None
            and raw_segment.font_size < self.min_font_size
        ):
            return False
        if (
            self.max_font_size is not None
            and raw_segment.font_size > self.max_font_size
        ):
            return False
        if (
            self.colours
            and not raw_segment.fill_colour.in_list(self.colours)
            and not raw_segment.stroke_colour.in_list(self.colours)
        ):
            return False
        if self.bold is not None and self.bold != raw_segment.bold:
            return False
        if self.italic is not None and self.italic != raw_segment.italic:
            return False
        if self.underline is not None and self.underline != raw_segment.underline:
            return False
        if self.strikethrough is not None and self.bold != raw_segment.strikethrough:
            return False
        return True


DEFAULT_FONT_MATCH = FontMatch()


def get_header_key(
    text: str, index_mapping: typing.Optional[typing.List[int]] = None
) -> str:
    output = ""
    for idx, char in enumerate(text):
        if char == " ":
            continue
        if index_mapping is not None:
            index_mapping.append(idx)
        output += char.lower()

    return output


class HashedTextFile:
    def __init__(self, path: typing.Union[str, pathlib.Path]) -> None:
        self.__path__ = pathlib.Path(path)
        self.__io__: typing.Optional[io.StringIO] = None

    def __enter__(self):
        self.__io__ = io.StringIO()
        return self

    def __exit__(self, ex_type, ex_value, ex_traceback):
        if not ex_type:
            text_bytes = self.__io__.getvalue().encode("utf8")

            if self.__path__.exists():
                write_file = False
                if self.__path__.stat().st_size != len(text_bytes):
                    write_file = True
                else:
                    with open(self.__path__, "rb") as fp:
                        write_file = (
                            hashlib.md5(fp.read()).digest()
                            != hashlib.md5(text_bytes).digest()
                        )
            else:
                write_file = True

            if write_file:
                with open(self.__path__, "wb") as fp:
                    fp.write(text_bytes)

    def write(self, value: str) -> int:
        return self.__io__.write(value)


class Colour(typing.NamedTuple):
    red: int = 0
    green: int = 0
    blue: int = 0

    def close_to(self, colour: Colour, fuzziness=2):
        return (
            abs(self.red - colour.red) <= fuzziness
            and abs(self.green - colour.green) <= fuzziness
            and abs(self.blue - colour.blue) <= fuzziness
        )

    def in_list(self, colours: typing.List[Colour], fuzziness=2):
        return any(self.close_to(colour, fuzziness) for colour in colours)


class BoundingBox(typing.NamedTuple):
    left: float
    top: float
    right: typing.Optional[float] = None
    bottom: typing.Optional[float] = None

    def within(self, other: BoundingBox, distance: float) -> bool:
        self_x_centre = (self.left + self.right) / 2
        self_y_centre = (self.top + self.bottom) / 2

        other_x_centre = (other.left + other.right) / 2
        other_y_centre = (other.top + other.bottom) / 2

        x_dist = (
            abs(self_x_centre - self.left) + distance + abs(other_x_centre - other.left)
        )
        y_dist = (
            abs(self_y_centre - self.top) + distance + abs(other_y_centre - other.top)
        )

        return (abs(self_x_centre - other_x_centre) <= x_dist) and (
            abs(self_y_centre - other_y_centre) <= y_dist
        )

    @staticmethod
    def from_list(
        other: typing.List[BoundingBox],
        left_to_right: bool = True,
        top_to_bottom: bool = True,
    ) -> BoundingBox:
        bbox_iter = iter(other)
        bbox = next(bbox_iter, None)

        if not bbox:
            return None
        left = bbox.left
        top = bbox.top
        right = bbox.right
        bottom = bbox.bottom

        for bbox in bbox_iter:
            if left_to_right:
                left = min(left, bbox.left)
                right = max(right, bbox.right)
            else:
                left = max(left, bbox.left)
                right = min(right, bbox.right)
            if top_to_bottom:
                top = min(top, bbox.top)
                bottom = max(bottom, bbox.bottom)
            else:
                top = max(top, bbox.top)
                bottom = min(bottom, bbox.bottom)
        return BoundingBox(left, top, right, bottom)


class StyleDefinition(typing.NamedTuple):
    fonts: typing.List[FontMatch] = []
    style: str = "paragraph"
    except_pages: PageMatch = DEFAULT_PAGE_MATCH
    only_pages: PageMatch = DEFAULT_PAGE_MATCH
    except_tags: typing.List[str] = []
    only_tags: typing.List[str] = []


class LayoutSection(typing.NamedTuple):
    x_bounds: typing.Tuple[int, int] = (0, 9999)
    y_bounds: typing.Tuple[int, int] = (0, 9999)
    handling: str = ""
    tag: str = ""

    def includes(self, left, top, right, bottom):
        return (
            self.x_bounds[0] <= left <= self.x_bounds[1]
            and self.y_bounds[0] <= top <= self.y_bounds[1]
            and self.x_bounds[0] <= right <= self.x_bounds[1]
            and self.y_bounds[0] <= bottom <= self.y_bounds[1]
        )

    def includes_bbox(self, bbox: BoundingBox):
        return self.includes(bbox.left, bbox.top, bbox.right, bbox.bottom)


DEFAULT_LAYOUT_SECTION = LayoutSection()


class LayoutDefinition(typing.NamedTuple):
    pages: PageMatch
    x_bounds: typing.Tuple[int, int] = (0, 9999)
    y_bounds: typing.Tuple[int, int] = (0, 9999)
    sections: typing.List[LayoutSection] = []
    notes: str = ""


class Extractor(typing.NamedTuple):
    pages: PageMatch = DEFAULT_PAGE_MATCH
    styles: typing.List[StyleDefinition] = []
    notes: str = ""


class Errata(typing.NamedTuple):
    pages: PageMatch
    find: str
    headings: typing.List[str]
    replace: typing.Optional[str] = None
    prepend_paragraph: typing.Optional[str] = None
    append_paragraph: typing.Optional[str] = None
    prepend_unsorted_list: typing.Optional[str] = None
    append_unsorted_list: typing.Optional[str] = None
    prepend_ordered_list: typing.Optional[str] = None
    append_ordered_list: typing.Optional[str] = None
    source: str = ""
    is_regex: bool = False
    notes: str = ""
    count: int = 1
    disable: typing.Optional[str] = None

    def __str__(self) -> str:
        action = (
            self.replace
            or self.prepend_paragraph
            or self.append_paragraph
            or self.prepend_unsorted_list
            or self.append_unsorted_list
            or self.prepend_ordered_list
            or self.append_ordered_list
        )
        return f"`{self.find}` => `{action}`, Headers: {', '.join(self.headings)}, Pages: {self.pages}"


class ErrataApplication(typing.NamedTuple):
    errata: Errata
    block: ExtractedBlock
    headers: typing.List[str]


class DocumentConfiguration(typing.NamedTuple):
    base_path: pathlib.Path
    file: pathlib.Path
    title: str
    authors: typing.List[str]
    input_config: typing.Dict[str, typing.Any]
    outputs: typing.Dict[str, typing.Dict[str, typing.Any]]
    extractors: typing.List[Extractor]
    errata: typing.List[Errata]
    transforms: typing.Dict[str, typing.Any]
    description: str = ""
    extractor: str = ""

    def __hash__(self) -> int:
        return make_hash(self)


class RawTextSegment(typing.NamedTuple):
    text: str
    font_name: str
    font_size: float
    bold: bool = False
    italic: bool = False
    underline: bool = False
    strikethrough: bool = False
    stroke_colour: Colour = Colour()
    fill_colour: Colour = Colour()
    link: str = ""
    bbox: typing.Optional[BoundingBox] = None
    paragraph_break: bool = False


class RawTextBlock(typing.NamedTuple):
    segments: typing.List[RawTextSegment]

    @property
    def text(self):
        return "".join([segment.text for segment in self.segments])

    def to_extracted(self, **kwargs) -> typing.Optional[ExtractedText]:
        text_skip = kwargs.pop("text_skip", 0)

        extracted_segments = []
        first = True
        for segment in self.segments:
            if first:
                text = segment.text[text_skip:]
                first = False
            else:
                text = segment.text

            if text:
                extracted_segments.append(
                    ExtractedTextSegment(
                        text=text,
                        bold=segment.bold,
                        italic=segment.italic,
                        underline=segment.underline,
                        strikethrough=segment.strikethrough,
                        stroke_colour=segment.stroke_colour,
                        fill_colour=segment.fill_colour,
                    )
                )

        if extracted_segments:
            return ExtractedText(extracted_segments)
        return None


class RawImage(typing.NamedTuple):
    file: pathlib.Path
    data: typing.Optional[bytes] = None
    callback: typing.Optional[typing.Callable[[], bytes]] = None
    description: str = ""

    def read_data(self):
        if self.data:
            return self.data
        if self.callback:
            return self.callback()
        with open(self.file, "rb") as fp:
            return fp.read()

    def to_extracted(self, **kwargs) -> ExtractedImage:
        return ExtractedImage(
            file=self.file,
            data=self.data,
            callback=self.callback,
            description=self.description,
        )


class RawTableCell(typing.NamedTuple):
    contents: typing.List[RawBlock]
    col_span: int = 1
    row_span: int = 1


class RawTableRow(typing.NamedTuple):
    cells: typing.List[RawTableCell]


class RawTable(typing.NamedTuple):
    width: int
    height: int
    rows: typing.List[RawTableRow]

    def to_extracted(self, **kwargs) -> ExtractedTable:
        return ExtractedTable(
            width=self.width,
            height=self.height,
            rows=[
                ExtractedTableRow(
                    rows=[
                        ExtractedTableCell(
                            col_span=cell.col_span,
                            row_span=cell.row_span,
                            value=[value.block.to_extracted() for value in cell.value],
                        )
                        for cell in row.cells
                    ]
                )
                for row in self.rows
            ],
        )


class RawBlock(typing.NamedTuple):
    block_type: str
    block: typing.Union[RawTextBlock, RawImage, RawTable]
    bbox: typing.Optional[BoundingBox] = None
    source: str = ""
    tag: str = ""


class RawSection(typing.NamedTuple):
    blocks: typing.List[RawBlock]
    layout: LayoutSection = DEFAULT_LAYOUT_SECTION


class RawPage(typing.NamedTuple):
    page_number: int
    sections: typing.List[RawSection]
    dropped: typing.List[RawBlock] = []
    width: typing.Optional[int] = None
    height: typing.Optional[int] = None

    def with_layout(
        self, document: RawDocument, layout: LayoutDefinition
    ) -> typing.List[RawBlock]:
        max_section = len(layout.sections)

        def layout_ordering(item: typing.Tuple[int, RawBlock]):
            index, raw_block = item
            bbox = raw_block.bbox
            if bbox is None:
                return (-1, 0, index)

            sorting_y = bbox.top if document.top_to_bottom_coords else -bbox.top
            sorting_x = bbox.left if document.left_to_right_coords else -bbox.left

            for index, section in enumerate(layout.sections):
                if section.includes_bbox(bbox):
                    return (index, sorting_y, sorting_x)
            return (max_section, sorting_y, sorting_x)

        min_x, max_x = layout.x_bounds
        min_y, max_y = layout.y_bounds

        for _, raw_block in sorted(enumerate(self.blocks), key=layout_ordering):
            bbox = raw_block.bbox
            if bbox is None:
                yield raw_block
                continue

            drop = False
            for section in layout.sections:
                if (
                    section.x_bounds[0] <= bbox.left <= section.x_bounds[1]
                    and section.y_bounds[0] <= bbox.top <= section.y_bounds[1]
                ):
                    if section.handling == HANDLING_DROP:
                        drop = True
                    break
            if drop:
                continue

            if document.top_to_bottom_coords:
                if min_y > raw_block.bbox.top or raw_block.bbox.bottom > max_y:
                    continue
            else:
                if min_y > raw_block.bbox.bottom or raw_block.bbox.top > max_y:
                    continue

            if document.left_to_right_coords:
                if min_x > raw_block.bbox.left or raw_block.bbox.right > max_x:
                    continue
            else:
                if min_x > raw_block.bbox.right or raw_block.bbox.left > max_x:
                    continue

            yield raw_block


class RawDocument(typing.NamedTuple):
    pages: typing.List[RawPage]
    rotation: int = 0
    top_to_bottom_coords: bool = True
    left_to_right_coords: bool = True


class ExtractedLink(typing.NamedTuple):
    external_link: typing.Optional[str] = None
    internal_headers: typing.List[ExtractedHeading] = []
    internal_page: typing.Optional[int] = None


class ExtractedTextSegment(typing.NamedTuple):
    text: str
    bold: bool = False
    italic: bool = False
    underline: bool = False
    strikethrough: bool = False
    fill_colour: Colour = Colour()
    stroke_colour: Colour = Colour()
    link: typing.Optional[ExtractedLink] = None

    def mutate(
        self,
        **kwargs,
    ):
        kwargs.setdefault("text", self.text)
        kwargs.setdefault("bold", self.bold)
        kwargs.setdefault("italic", self.italic)
        kwargs.setdefault("underline", self.underline)
        kwargs.setdefault("strikethrough", self.strikethrough)
        kwargs.setdefault("fill_colour", self.fill_colour)
        kwargs.setdefault("stroke_colour", self.stroke_colour)
        kwargs.setdefault("link", self.link)
        return ExtractedTextSegment(**kwargs)


class ExtractedText(typing.NamedTuple):
    segments: typing.List[ExtractedTextSegment]

    def join(self):
        return MULTIPLE_WHITESPACE_RE.sub(
            " ", "".join(segment.text for segment in self.segments).strip()
        )


class ExtractedTableCell(typing.NamedTuple):
    contents: typing.List[ExtractedBlock]
    col_span: int = 1
    row_span: int = 1


class ExtractedTableRow(typing.NamedTuple):
    cells: typing.List[ExtractedTableCell]


class ExtractedTable(typing.NamedTuple):
    width: int
    height: int
    rows: typing.List[ExtractedTableRow]


class ExtractedImage(typing.NamedTuple):
    file: pathlib.Path
    data: typing.Optional[bytes] = None
    callback: typing.Optional[typing.Callable[[], bytes]] = None
    description: str = ""

    def read_data(self):
        if self.data:
            return self.data
        if self.callback:
            return self.callback()
        with open(self.file, "rb") as fp:
            return fp.read()


class ExtractedBlock(typing.NamedTuple):
    style: str
    block: typing.Union[ExtractedText, ExtractedTable, ExtractedImage]
    page_number: int = 0
    source: str = ""
    tag: str = ""


class ExtractedHeading:
    def __init__(
        self,
        parent: typing.Optional[ExtractedHeading],
        entries: typing.Optional[typing.List[ExtractedBlock]] = None,
        subheadings: typing.Optional[typing.List[ExtractedHeading]] = None,
    ) -> None:
        self.parent = parent
        self.entries = entries or []
        self.subheadings = subheadings or []
        self.output_filename: typing.Optional[str] = None
        self.rendered_filename: typing.Optional[str] = None
        self.anchor: typing.Optional[str] = None

    @property
    def block(self) -> ExtractedBlock:
        return self.entries[0]

    @property
    def title(self) -> str:
        return propercase(self.block.block.join())

    @property
    def level(self) -> int:
        if self.parent is None:
            return 1
        return self.parent.level + 1

    def get_blocks(self, min_header_level: int) -> typing.List[ExtractedBlock]:
        blocks = [self.entries[0]]
        for block in self.entries[1:]:
            header_level = HEADER_LEVEL_LOOKUP.get(block.style)
            if header_level and header_level <= min_header_level:
                break
            blocks.append(block)
        return blocks

    def get_heirarchy(self) -> typing.List[ExtractedHeading]:
        header_node = self
        header_heirarchy = []

        while header_node is not None:
            header_heirarchy.append(header_node)
            header_node = header_node.parent
        header_heirarchy.reverse()

        return header_heirarchy


class ExtractedDocument:
    def __init__(
        self,
        blocks: typing.Optional[typing.List[ExtractedBlock]] = None,
        available_errata: typing.Optional[typing.List[Errata]] = None,
        applied_errata: typing.Optional[
            typing.List[typing.List[ErrataApplication]]
        ] = None,
        unapplied_errata: typing.Optional[typing.List[Errata]] = None,
    ) -> None:
        self.entries: typing.List[ExtractedBlock] = blocks or []
        self.headings: typing.List[ExtractedHeading] = []
        self.heading_lookup: typing.Dict[str, typing.List[ExtractedHeading]] = {}
        self.available_errata: typing.List[Errata] = available_errata or []
        self.applied_errata: typing.List[typing.List[ErrataApplication]] = (
            applied_errata or []
        )
        self.unapplied_errata: typing.List[Errata] = unapplied_errata or []
        self.refresh_headings()

    def refresh_headings(self):
        def extract_heading(
            start_block_idx,
            header_level,
            parent: typing.Optional[ExtractedHeading] = None,
        ):
            heading = ExtractedHeading(parent, [self.entries[start_block_idx]])
            self.headings.append(heading)

            block_idx = start_block_idx + 1
            while block_idx < len(self.entries):
                extracted_block = self.entries[block_idx]
                cur_header_level = HEADER_LEVEL_LOOKUP.get(extracted_block.style)
                if cur_header_level is not None:
                    if cur_header_level <= header_level:
                        break
                    block_idx, subheading = extract_heading(
                        block_idx, cur_header_level, heading
                    )
                    heading.entries += subheading.entries
                    heading.subheadings.append(subheading)
                else:
                    heading.entries.append(extracted_block)
                    block_idx += 1

            return block_idx, heading

        self.headings = []
        block_idx = 0
        while block_idx < len(self.entries):
            extracted_block = self.entries[block_idx]
            header_level = HEADER_LEVEL_LOOKUP.get(extracted_block.style)
            if header_level is not None:
                block_idx = extract_heading(block_idx, header_level)[0]
            else:
                block_idx += 1

        self.heading_lookup = {}
        for heading in self.headings:
            self.heading_lookup.setdefault(
                get_header_key(heading.block.block.join()), []
            ).append(heading)

    def get_headers_by_name(self, name: str) -> typing.List[str]:
        key = name.replace(" ", "").lower()
        return self.heading_lookup.get(get_header_key(name))


MAC_WORDS = set(["machine"])

MC_WORDS = set([])


def propercase(text: str):
    if not text:
        return text
    output = ""

    pos = 0
    for match in PROPER_CASE_RE.finditer(text):
        start = match.start(0)
        if pos < start:
            output += text[pos:start]

        match_text_orig = match.group(0)
        match_text = match_text_orig.lower()

        if (
            match_text.startswith("mac")
            and match_text not in MAC_WORDS
            and len(match_text) > 3
        ):
            output += "Mac" + match_text[3].upper() + match_text[4:]
        elif (
            match_text.startswith("mc")
            and match_text not in MC_WORDS
            and len(match_text) > 2
        ):
            output += "Mc" + match_text[2].upper() + match_text[3:]
        elif match_text in ["a", "von", "de", "du", "of"]:
            output += match_text
        else:
            output += match.group(1).upper()
            output += match.group(2).lower()

        pos = match.end(0)

    output += text[pos:]
    return output[0].upper() + output[1:]


def build_extractors(
    extract_conf: typing.Dict[str, typing.Any]
) -> typing.List[Extractor]:
    extractors: typing.List[Extractor] = []
    extractor_def: typing.Dict[str, typing.Any]
    extractor_defs = extract_conf.get("extractors", [])
    if not extractor_defs:
        extractor_defs.append({})
    for extractor_def in extractor_defs:
        styles = []
        style: typing.Dict[str, typing.Any]
        for style in extractor_def.get("styles", []):
            style.setdefault("style", "paragraph")
            fonts = []
            for font in style.get("fonts", []):
                font["colours"] = [
                    Colour(**colour) if isinstance(colour, dict) else Colour(*colour)
                    for colour in font.get("colours", [])
                ]
                fonts.append(FontMatch(**font))

            style["fonts"] = fonts
            style["only_pages"] = PageMatch(style.get("only_pages"))
            style["except_pages"] = PageMatch(style.get("except_pages", []))
            try:
                styles.append(StyleDefinition(**style))
            except TypeError as e:
                print(f"Exception {e} in {style}")
                raise e
            if style.get("style") not in STYLE_NAME_LOOKUP:
                raise ValueError(f"Unknown style: {style.get('style')}")

        if not styles:
            styles.append(StyleDefinition(style="paragraph"))

        extractor_def["pages"] = PageMatch(extractor_def.get("pages"))
        extractor_def["styles"] = styles

        extractors.append(Extractor(**extractor_def))

    return extractors


def build_layouts(
    layout_defs: typing.List[typing.Dict[str, typing.Any]]
) -> typing.List[LayoutDefinition]:
    layouts: typing.List[LayoutDefinition] = []
    layout_def: typing.Dict[str, typing.Any]
    if not layout_defs:
        layout_defs.append({})

    for layout_def in layout_defs:
        if isinstance(layout_def, LayoutDefinition):
            layouts.append(layout_def)
        else:
            sections = []
            for section_block in layout_def.get("sections", []):
                if isinstance(section_block, LayoutSection):
                    sections.append(section_block)
                else:
                    section_block.setdefault("x_bounds", [0, 9999])
                    section_block.setdefault("y_bounds", [0, 9999])
                    sections.append(LayoutSection(**section_block))

            if not sections:
                sections.append(LayoutSection(x_bounds=[0, 9999], y_bounds=[0, 9999]))

            layout_def["pages"] = PageMatch(layout_def.get("pages"))
            layout_def.setdefault("x_bounds", [0, 9999])
            layout_def.setdefault("y_bounds", [0, 9999])
            layout_def["sections"] = sections

            layouts.append(LayoutDefinition(**layout_def))

    return layouts


def load_document_config(
    conf_file: str, source_file: pathlib.Path
) -> DocumentConfiguration:

    with open(conf_file, "r", encoding="utf8") as fp:
        extract_conf: typing.Dict[str, typing.Any] = json.load(fp)

    base_path = pathlib.Path(conf_file).parent

    extract_conf["base_path"] = base_path
    extract_conf["file"] = source_file
    extract_conf["extractors"] = build_extractors(extract_conf)
    extract_conf.setdefault("input_config", {})
    extract_conf.setdefault("outputs", {})

    errata_list = []
    for errata in extract_conf.get("errata", []):
        if "heading" in errata:
            errata["headings"] = [errata.pop("heading")]

        if "headings" not in errata:
            errata["headings"] = []
        else:
            errata["headings"] = [
                NOT_WORDS_RE.sub("", heading.lower() or "")
                for heading in errata["headings"]
            ]
        errata["pages"] = PageMatch(errata.get("pages"))
        errata_list.append(Errata(**errata))
    extract_conf["errata"] = errata_list
    extract_conf.setdefault("transforms", {})

    return DocumentConfiguration(**extract_conf)
