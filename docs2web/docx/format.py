import typing

import docx
import docx.document
from docx.text.paragraph import Paragraph
from docx.table import Table

from .. import common


DEFAULT_FONT_SIZE = 8


def get_font_size(paragraph_style, run_style=None):
    font_size = 0
    if paragraph_style.base_style:
        font_size = get_font_size(paragraph_style.base_style)
    if paragraph_style and paragraph_style.font.size is not None:
        font_size = paragraph_style.font.size.pt
    if run_style and run_style.size is not None:
        if run_style.base_style:
            font_size = get_font_size(run_style.base_style)
        font_size = run_style.size.pt
    return font_size


def get_raw_text_segment(paragraph, run):
    text = ""
    bold = False
    italic = False
    underline = False
    strikethrough = False

    if paragraph:
        if paragraph.style.font.bold is not None:
            bold = paragraph.style.font.bold
        if paragraph.style.font.italic is not None:
            italic = paragraph.style.font.italic
        if paragraph.style.font.underline is not None:
            underline = paragraph.style.font.underline
        if paragraph.style.font.strike is not None:
            strikethrough = paragraph.style.font.strike

    if run:
        text = run.text
        if run.font.bold is not None:
            bold = run.font.bold
        if run.font.italic is not None:
            italic = run.font.italic
        if run.font.underline is not None:
            underline = run.font.underline
        if run.style.font.strike is not None:
            strikethrough = run.style.font.strike

    return common.RawTextSegment(
        text,
        font_name="Noname",
        font_size=get_font_size(paragraph.style) or DEFAULT_FONT_SIZE,
        bold=bold,
        italic=italic,
        underline=underline,
        strikethrough=strikethrough,
    )


def extract_raw_document(
    extract_conf: common.DocumentConfiguration, **kwargs
) -> common.RawDocument:
    pages = []

    document = docx.Document(str(extract_conf.file))

    page_number = 1
    next_page_number = 1
    elements: typing.List[common.RawBlock] = []

    for element in document._body._element:
        if element.tag.endswith("}p"):
            paragraph = Paragraph(element, document._body)

            page_number = next_page_number
            for run in paragraph.runs:
                for element in run.element:
                    if element.tag.endswith("}br"):
                        next_page_number += 1

            if page_number != next_page_number:
                pages.append(
                    common.RawPage(
                        page_number=page_number,
                        sections=[common.RawSection(blocks=elements)],
                    )
                )
                elements = []

            if paragraph.text.strip():
                elements.append(
                    common.RawBlock(
                        block_type=common.RAW_TEXT_BLOCK,
                        block=common.RawTextBlock(
                            segments=[
                                get_raw_text_segment(paragraph, run)
                                for run in paragraph.runs
                            ]
                        ),
                    )
                )
        elif element.tag.endswith("}tbl"):
            table = Table(element, document._body)
            rows = table.rows

            extracted_rows: typing.List[common.RawTableRow] = []
            for row in rows:
                extracted_cells: typing.List[common.RawTableCell] = []
                for cell in row.cells:
                    extracted_cells.append(
                        common.RawTableCell(
                            col_span=1,
                            row_span=1,
                            contents=[
                                common.RawBlock(
                                    common.RAW_TEXT_BLOCK,
                                    common.RawTextBlock(
                                        segments=[
                                            common.RawTextSegment(
                                                cell.text, "Noname", DEFAULT_FONT_SIZE
                                            )
                                        ]
                                    ),
                                )
                            ],
                        )
                    )
                extracted_rows.append(common.RawTableRow(extracted_cells))

            elements.append(
                common.RawBlock(
                    block_type=common.RAW_TABLE_BLOCK,
                    block=common.RawTable(
                        width=table._column_count, height=len(rows), rows=extracted_rows
                    ),
                )
            )
    return common.RawDocument(pages=pages)
