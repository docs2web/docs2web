from __future__ import annotations

import typing
import pathlib
import time


class CacheEntry(typing.NamedTuple):
    item: typing.Any
    last_access: float
    file_path: typing.Optional[pathlib.Path]
    file_mtime: typing.Optional[float]
    unload_func: typing.Optional[
        typing.Callable[[str, typing.Optional[pathlib.Path], typing.Any], None]
    ]
    evictable: bool

    def update_last_access(self, last_access: float) -> CacheEntry:
        return CacheEntry(
            item=self.item,
            last_access=last_access,
            file_path=self.file_path,
            file_mtime=self.file_mtime,
            unload_func=self.unload_func,
            evictable=self.evictable,
        )


__cache__: typing.Dict[str, CacheEntry] = {}
__max_cache_size__ = 5


def get_cache_item(
    item_id: str,
    path: typing.Optional[pathlib.Path],
    load_func: typing.Callable[[str, typing.Optional[pathlib.Path]], typing.Any],
    unload_func: typing.Optional[
        typing.Callable[[str, typing.Optional[pathlib.Path], typing.Any], None]
    ] = None,
    can_evict: bool = True,
):
    if path is not None and path.exists():
        mtime = path.stat().st_mtime
    else:
        mtime = None

    last_access = time.time()

    cache_item: typing.Optional[CacheEntry] = __cache__.get(item_id, None)

    if cache_item is not None and mtime is not None:
        if cache_item.file_mtime != mtime:
            if cache_item.unload_func is not None:
                cache_item.unload_func(item_id, cache_item.file_path, cache_item.item)
            cache_item = None

    if cache_item is None:
        item = load_func(item_id, path)
        cache_item = CacheEntry(
            item=item,
            last_access=last_access,
            file_path=path,
            file_mtime=mtime,
            unload_func=unload_func,
            evictable=can_evict,
        )
        __cache__[item_id] = cache_item

        if len(__cache__) > __max_cache_size__:
            candidate_id: typing.Optional[str] = None
            candidate: typing.Optional[CacheEntry] = None

            for existing_item_id, existing_item in __cache__.items():
                if not existing_item.evictable:
                    continue

                if existing_item_id == item_id:
                    continue

                if (
                    candidate is None
                    or candidate.last_access > existing_item.last_access
                ):
                    candidate_id = existing_item_id
                    candidate = existing_item

            if candidate_id is not None:
                del __cache__[candidate_id]
                if candidate.unload_func is not None:
                    candidate.unload_func(
                        candidate_id, candidate.file_path, candidate.item
                    )
    else:
        __cache__[item_id] = cache_item.update_last_access(last_access)

    return cache_item.item
