import typing

from . import common


def raw_block_to_extracted_block(
    extractor: common.Extractor,
    document: common.RawDocument,
    page: common.RawPage,
    raw_block: common.RawBlock,
    selected_style: typing.Optional[str] = None,
    text_skip: typing.Optional[int] = None,
):
    if raw_block.block_type == common.RAW_TABLE_BLOCK:
        return common.ExtractedBlock(
            style=common.TABLE,
            block=common.ExtractedTable(
                width=raw_block.block.width,
                height=raw_block.block.height,
                rows=[
                    common.ExtractedTableRow(
                        cells=[
                            common.ExtractedTableCell(
                                col_span=cell.col_span,
                                row_span=cell.row_span,
                                contents=raw_blocks_to_extracted_blocks(
                                    document, page, cell.contents, extractor
                                ),
                            )
                            for cell in row.cells
                        ]
                    )
                    for row in raw_block.block.rows
                ],
            ),
            page_number=page.page_number,
            source=f"Extractor: {extractor.notes}\n{str(raw_block.bbox)} {raw_block.block}",
            tag=raw_block.tag,
        )
    elif raw_block.block_type == common.RAW_IMAGE_BLOCK:
        return common.ExtractedBlock(
            style=common.IMAGE,
            block=raw_block.block.to_extracted(),
            page_number=page.page_number,
            source=f"Extractor: {extractor.notes}\n{str(raw_block.bbox)} {raw_block.block}",
            tag=raw_block.tag,
        )
    else:
        text_block = raw_block.block.to_extracted(text_skip=text_skip)
        if text_block:
            return common.ExtractedBlock(
                style=selected_style,
                block=text_block,
                page_number=page.page_number,
                source=f"Extractor: {extractor.notes}\n{str(raw_block.bbox)} {raw_block.block}",
                tag=raw_block.tag,
            )
    return None


def raw_blocks_to_extracted_blocks(
    document: common.RawDocument,
    page: common.RawPage,
    raw_blocks: typing.List[common.RawBlock],
    extractor: common.Extractor,
) -> typing.List[common.ExtractedBlock]:
    extracted_blocks: typing.List[common.ExtractedBlock] = []
    next_style = None

    split_blocks: typing.List[common.RawBlock] = []
    for raw_block in raw_blocks:
        if raw_block.block_type != common.RAW_TEXT_BLOCK:
            split_blocks.append(raw_block)
            continue

        new_blocks: typing.List[typing.List[common.RawTextSegment]] = []
        cur_block: typing.List[common.RawTextSegment] = []
        last_style = None

        for segment in raw_block.block.segments:
            style: common.StyleDefinition
            selected_style = None
            for style in extractor.styles:
                if page.page_number in style.except_pages:
                    continue
                if style.only_pages and page.page_number not in style.only_pages:
                    continue
                if style.only_tags and raw_block.tag not in style.only_tags:
                    continue
                if raw_block.tag in style.except_tags:
                    continue

                match_font = not bool(style.fonts)
                for font in style.fonts:
                    if font.matches(segment):
                        match_font = True
                        break

                if match_font:
                    selected_style = common.STYLE_NAME_LOOKUP.get(style.style)
                    break

            if last_style and last_style != selected_style:
                if cur_block:
                    new_blocks.append(cur_block)
                    cur_block = []

            last_style = selected_style
            if selected_style:
                cur_block.append(segment)
            else:
                print(f"Dropping segment: {segment}")

        if cur_block:
            new_blocks.append(cur_block)

        if len(new_blocks) > 1:
            for segments in new_blocks:
                split_blocks.append(
                    common.RawBlock(
                        block_type=common.RAW_TEXT_BLOCK,
                        block=common.RawTextBlock(segments),
                        bbox=raw_block.bbox,
                        source=raw_block.source,
                        tag=raw_block.tag,
                    )
                )
        elif len(new_blocks) == 1:
            split_blocks.append(raw_block)

    for raw_block in split_blocks:
        selected_style = None
        text_skip = 0

        if next_style is not None:
            selected_style = next_style
            next_style = None
        elif raw_block.block_type == common.RAW_TEXT_BLOCK:
            text_block: common.RawTextBlock = raw_block.block
            if not text_block.segments:
                continue

            first_segment = text_block.segments[0]

            style: common.StyleDefinition
            for style in extractor.styles:
                if page.page_number in style.except_pages:
                    continue
                if style.only_pages and page.page_number not in style.only_pages:
                    continue
                if style.only_tags and raw_block.tag not in style.only_tags:
                    continue
                if raw_block.tag in style.except_tags:
                    continue

                match_font = not bool(style.fonts)
                for font in style.fonts:
                    if font.matches(first_segment):
                        match_font = True
                        break

                if match_font:
                    selected_style = common.STYLE_NAME_LOOKUP.get(style.style)
                    break

        if selected_style is None and raw_block.block_type == common.RAW_TEXT_BLOCK:
            print(
                f"Could not find style for: {raw_block} @ Page #{page.page_number}, Extractor: {extractor.notes}"
            )
            continue

        if selected_style == common.SKIP:
            continue

        if raw_block:
            block = raw_block_to_extracted_block(
                extractor,
                document,
                page,
                raw_block,
                selected_style,
                text_skip,
            )
            if block:
                extracted_blocks.append(block)

    return extracted_blocks


def extract_blocks(
    document: common.RawDocument,
    extract_conf: common.DocumentConfiguration,
) -> typing.List[common.ExtractedBlock]:
    mapped_extractors: typing.Dict[int, common.Extractor] = {}
    for extractor in extract_conf.extractors:
        extractor.pages.apply_mappings(
            mapped_extractors, extractor, len(document.pages)
        )

    extracted_blocks: typing.List[common.ExtractedBlock] = []

    for page in document.pages:
        extractor: common.Extractor = mapped_extractors.get(page.page_number)
        if not extractor:
            continue

        for section in page.sections:
            new_blocks = raw_blocks_to_extracted_blocks(
                document, page, section.blocks, extractor
            )
            extracted_blocks += new_blocks

    return extracted_blocks
