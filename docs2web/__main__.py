from __future__ import annotations
import typing
import pathlib
import time
import logging

import click

from . import common
from . import transform
from .extract import extract_blocks
from .visualise import visualise_as_html
from .mdbook import output_mdbook
from .docusaurus import output_docusaurus

logging.basicConfig(level=logging.WARN)


def get_extractor(
    extract_conf: common.DocumentConfiguration,
) -> typing.Callable[[common.DocumentConfiguration], common.RawDocument]:
    filename = extract_conf.file
    extractor = extract_conf.extractor.lower()
    if extractor == "pdf" or filename.name.lower().endswith(".pdf"):
        from . import pdf

        return pdf.extract_raw_document
    elif extractor == "docx" or filename.name.lower().endswith(".docx"):
        from . import docx

        return docx.extract_raw_document
    elif extractor == "hocr" or filename.name.lower().endswith(".hocr"):
        from . import hocr

        return hocr.extract_raw_document
    elif extractor == "odt" or filename.name.lower().endswith(".odt"):
        from . import odt

        return odt.extract_raw_document
    raise ValueError("Cannot handle this kind of file")


@click.group()
def cli():
    pass


@cli.command()
@click.argument("conf_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("source_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("out_file", type=click.Path(path_type=pathlib.Path))
def dump_raw(
    conf_file: pathlib.Path, source_file: pathlib.Path, out_file: pathlib.Path
):
    click.echo("Loading configuration")
    extract_conf = common.load_document_config(conf_file, source_file)
    extract_func = get_extractor(extract_conf)

    click.echo("Extracting document")
    document: common.RawDocument = extract_func(extract_conf)

    with out_file.open("w", encoding="utf8") as fp:
        for page in document.pages:
            fp.write(f"Page {page.page_number}:\n")
            for section_idx, section in enumerate(page.sections):
                fp.write(f"  Section {section_idx}")
                for element in section.blocks:
                    if element.block_type == common.RAW_TEXT_BLOCK:
                        fp.write("    TextBlock\n")
                        for raw_text in element.block.segments:
                            font_style = ""
                            if raw_text.bold:
                                font_style += "B"
                            if raw_text.italic:
                                font_style += "I"
                            if raw_text.underline:
                                font_style += "U"
                            if raw_text.strikethrough:
                                font_style += "S"
                            if font_style:
                                font_style = "/" + font_style
                            fp.write(
                                f"      Text ({raw_text.font_name}:{raw_text.font_size}{font_style}): {repr(raw_text.text)}\n"
                            )
                    else:
                        fp.write(f"    Unknown Type: {element.block_type}\n")


@cli.command()
@click.argument("conf_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("source_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.option("--ignore-font-size", is_flag=True)
def dump_stats(
    conf_file: pathlib.Path, source_file: pathlib.Path, ignore_font_size: bool
):
    click.echo("Loading configuration")
    extract_conf = common.load_document_config(conf_file, source_file)
    extract_func = get_extractor(extract_conf)

    click.echo("Extracting document")
    document: common.RawDocument = extract_func(extract_conf)
    click.echo(f"Page Count: {len(document.pages)}")

    font_counts = {}
    for page in document.pages:
        for section in page.sections:
            for element in section.blocks:
                if element.block_type == common.RAW_TEXT_BLOCK:
                    for segment in element.block.segments:
                        if ignore_font_size:
                            key = segment.font_name
                        else:
                            key = f"{segment.font_name}:{segment.font_size}"
                        font_counts[key] = font_counts.get(key, 0) + 1

    click.echo(f"Fonts (found {len(font_counts)} unique):")
    for font_key, count in sorted(
        font_counts.items(), key=lambda item: (item[1], item[0]), reverse=True
    ):
        click.echo(f"  {font_key} = {count}")


@cli.command()
@click.argument("conf_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("source_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("out_file", type=click.Path(path_type=pathlib.Path))
@click.option("--include-transforms", is_flag=True)
def dump_extracted(
    conf_file: pathlib.Path,
    source_file: pathlib.Path,
    out_file: pathlib.Path,
    include_transforms: bool,
):
    click.echo("Loading configuration")
    extract_conf = common.load_document_config(conf_file, source_file)
    extract_func = get_extractor(extract_conf)

    click.echo("Extracting document")
    document: common.RawDocument = extract_func(extract_conf)
    document = transform.apply_pre_extract_transforms(extract_conf, document)
    extracted_blocks = extract_blocks(document, extract_conf)

    if include_transforms:
        extracted_blocks = transform.apply_post_extract_transforms(
            extract_conf, extracted_blocks, True
        ).entries

    with out_file.open("w", encoding="utf8") as fp:
        for block in extracted_blocks:
            fp.write(f"Block: {block}\n")


@cli.command()
@click.option("--watch", is_flag=True)
@click.argument("conf_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("source_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument(
    "out_dir", type=click.Path(file_okay=False, dir_okay=True, path_type=pathlib.Path)
)
@click.option("--page-zoom", type=float, default=1.0)
@click.option("--with-images", is_flag=True)
def visualise(
    watch: bool,
    conf_file: pathlib.Path,
    source_file: pathlib.Path,
    out_dir: pathlib.Path,
    page_zoom: float,
    with_images: bool,
):
    conf_mtime = conf_file.stat().st_mtime
    click.echo("Loading configuration")
    extract_conf = common.load_document_config(conf_file, source_file)

    # For PDF documents, ignore table handling
    extract_conf.input_config.pop("table_handling", None)
    extract_func = get_extractor(extract_conf)

    click.echo("Loading document")
    doc_mtime = extract_conf.file.stat().st_mtime
    document: common.RawDocument = extract_func(extract_conf, process_dropped=True)

    first_render = True

    while True:
        click.echo("Visualising as HTML...")
        visualise_as_html(
            extract_conf.title,
            extract_conf,
            document,
            out_dir,
            page_zoom=page_zoom,
            draw_images=with_images,
        )

        if first_render:
            click.echo("Visualization complete, launching browser")
            click.launch(str(out_dir / "index.html"))
            first_render = False
        else:
            click.echo("Visualization refreshed")

        if not watch:
            break

        while True:
            time.sleep(1.0)
            new_conf_mtime = conf_file.stat().st_mtime
            new_doc_mtime = extract_conf.file.stat().st_mtime

            if new_conf_mtime <= conf_mtime and new_doc_mtime <= doc_mtime:
                continue

            if new_conf_mtime > conf_mtime:
                conf_mtime = new_conf_mtime
                click.echo("Reloading configuration")
                try:
                    new_extract_conf = common.load_document_config(
                        conf_file, source_file
                    )

                    # For PDF documents, ignore table handling
                    new_extract_conf.input_config.pop("table_handling", None)
                except Exception as e:
                    click.echo(
                        f"Unable to load configuration, please check: {str(e)}",
                        err=True,
                    )
                    continue

                pre_hash = common.make_hash(extract_conf.input_config)
                post_hash = common.make_hash(new_extract_conf.input_config)
                if pre_hash != post_hash:
                    doc_mtime = -1

                extract_conf = new_extract_conf

            if new_doc_mtime > doc_mtime:
                doc_mtime = new_doc_mtime
                extract_func = get_extractor(extract_conf)

                click.echo("Reloading document")

                try:
                    new_document: common.RawDocument = extract_func(
                        extract_conf, process_dropped=True
                    )
                except Exception as e:
                    click.echo(
                        f"Unable to load document, please check: {str(e)}", err=True
                    )
                    continue
                document = new_document

            break


@cli.command()
@click.option("--watch", is_flag=True)
@click.option("--output-debug", is_flag=True)
@click.option("--skip-errata", is_flag=True)
@click.argument("conf_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("source_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument(
    "out_dir", type=click.Path(file_okay=False, dir_okay=True, path_type=pathlib.Path)
)
def mdbook(
    watch: bool,
    output_debug: bool,
    skip_errata: bool,
    conf_file: pathlib.Path,
    source_file: pathlib.Path,
    out_dir: pathlib.Path,
):
    conf_mtime = conf_file.stat().st_mtime
    click.echo("Loading configuration")
    extract_conf = common.load_document_config(conf_file, source_file)

    extract_func = get_extractor(extract_conf)

    click.echo("Loading document")
    doc_mtime = extract_conf.file.stat().st_mtime
    document: common.RawDocument = extract_func(extract_conf)
    document = transform.apply_pre_extract_transforms(extract_conf, document)

    while True:
        click.echo("Extracting elements")
        extracted_blocks = extract_blocks(document, extract_conf)
        extracted_document = transform.apply_post_extract_transforms(
            extract_conf, extracted_blocks, skip_errata=skip_errata
        )

        click.echo("Building mdbook")
        output_mdbook(
            extract_conf,
            extract_conf.outputs.get("mdbook", {}),
            extracted_document,
            out_dir,
            output_debug=output_debug,
        )

        if not watch:
            break

        click.echo("Waiting for changes...")

        while True:
            time.sleep(1.0)
            new_conf_mtime = conf_file.stat().st_mtime
            new_doc_mtime = extract_conf.file.stat().st_mtime

            if new_conf_mtime <= conf_mtime and new_doc_mtime <= doc_mtime:
                continue

            if new_conf_mtime > conf_mtime:
                conf_mtime = new_conf_mtime
                click.echo("Reloading configuration")
                try:
                    new_extract_conf = common.load_document_config(
                        conf_file, source_file
                    )
                except Exception as e:
                    click.echo(
                        f"Unable to load configuration, please check: {str(e)}",
                        err=True,
                    )
                    continue

                pre_hash = common.make_hash(extract_conf.input_config)
                post_hash = common.make_hash(new_extract_conf.input_config)
                if pre_hash != post_hash:
                    doc_mtime = -1

                extract_conf = new_extract_conf

            if new_doc_mtime > doc_mtime:
                doc_mtime = new_doc_mtime
                extract_func = get_extractor(extract_conf)

                click.echo("Reloading document")

                try:
                    new_document: common.RawDocument = extract_func(extract_conf)
                except Exception as e:
                    click.echo(
                        f"Unable to load document, please check: {str(e)}", err=True
                    )
                    continue
                document = transform.apply_pre_extract_transforms(
                    extract_conf, new_document
                )

            break


@cli.command()
@click.option("--watch", is_flag=True)
@click.option("--output-debug", is_flag=True)
@click.option("--skip-errata", is_flag=True)
@click.option("--markdown-base", default="src/pages")
@click.option("--image-base", default="img")
@click.option("--prefix", default="")
@click.option("--override-title", default=None)
@click.option("--position", default=None, type=int)
@click.argument("conf_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument("source_file", type=click.Path(exists=True, path_type=pathlib.Path))
@click.argument(
    "out_dir", type=click.Path(file_okay=False, dir_okay=True, path_type=pathlib.Path)
)
def docusaurus(
    watch: bool,
    output_debug: bool,
    skip_errata: bool,
    conf_file: pathlib.Path,
    source_file: pathlib.Path,
    out_dir: pathlib.Path,
    markdown_base: str,
    image_base: str,
    prefix: str,
    override_title: typing.Optional[str] = None,
    position: typing.Optional[int] = None,
):
    conf_mtime = conf_file.stat().st_mtime
    click.echo("Loading configuration")
    extract_conf = common.load_document_config(conf_file, source_file)

    extract_func = get_extractor(extract_conf)

    click.echo("Loading document")
    doc_mtime = extract_conf.file.stat().st_mtime
    document: common.RawDocument = extract_func(extract_conf)
    document = transform.apply_pre_extract_transforms(extract_conf, document)

    while True:
        click.echo("Extracting elements")
        extracted_blocks = extract_blocks(document, extract_conf)
        extracted_document = transform.apply_post_extract_transforms(
            extract_conf, extracted_blocks, skip_errata=skip_errata
        )

        click.echo("Building docusaurus")
        output_docusaurus(
            extract_conf,
            extract_conf.outputs.get("docusaurus", {}),
            extracted_document,
            out_dir,
            markdown_base,
            image_base,
            prefix,
            output_debug=output_debug,
            override_title=override_title,
            position=position,
        )

        if not watch:
            break

        click.echo("Waiting for changes...")

        while True:
            time.sleep(1.0)
            new_conf_mtime = conf_file.stat().st_mtime
            new_doc_mtime = extract_conf.file.stat().st_mtime

            if new_conf_mtime <= conf_mtime and new_doc_mtime <= doc_mtime:
                continue

            if new_conf_mtime > conf_mtime:
                conf_mtime = new_conf_mtime
                click.echo("Reloading configuration")
                try:
                    new_extract_conf = common.load_document_config(
                        conf_file, source_file
                    )
                except Exception as e:
                    click.echo(
                        f"Unable to load configuration, please check: {str(e)}",
                        err=True,
                    )
                    continue

                pre_hash = common.make_hash(extract_conf.input_config)
                post_hash = common.make_hash(new_extract_conf.input_config)
                if pre_hash != post_hash:
                    doc_mtime = -1

                extract_conf = new_extract_conf

            if new_doc_mtime > doc_mtime:
                doc_mtime = new_doc_mtime
                extract_func = get_extractor(extract_conf)

                click.echo("Reloading document")

                try:
                    new_document: common.RawDocument = extract_func(extract_conf)
                except Exception as e:
                    click.echo(
                        f"Unable to load document, please check: {str(e)}", err=True
                    )
                    continue
                document = transform.apply_pre_extract_transforms(
                    extract_conf, new_document
                )

            break


if __name__ == "__main__":
    cli()
