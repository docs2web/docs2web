import json
import typing
import pathlib

from . import common
from .markdown import output_markdown, CommonMarkStyle


class MdBookStyle(CommonMarkStyle):
    def __init__(self, file_header_level: int) -> None:
        self.file_header_level = file_header_level
        super().__init__()

    def get_rendered_name(self, output_filename: pathlib.Path) -> typing.Optional[str]:
        return str(output_filename.with_suffix(".html"))


def output_mdbook(
    extract_conf: common.DocumentConfiguration,
    output_config: typing.Dict[str, typing.Any],
    extracted_document: common.ExtractedDocument,
    output_root: pathlib.Path,
    *,
    output_debug: bool = False,
):
    file_header_level = output_config.get("file_header_level", 2)

    source_dir = output_root / "src"
    source_dir.mkdir(parents=True, exist_ok=True)

    output_markdown(
        extract_conf,
        MdBookStyle(file_header_level),
        extracted_document,
        source_dir,
        output_debug=output_debug,
        file_header_level=file_header_level,
    )

    with common.HashedTextFile(source_dir / "SUMMARY.md") as out_fp:
        out_fp.write("# Summary\n\n")
        for item in extracted_document.headings:
            header_level = common.HEADER_LEVEL_LOOKUP.get(item.block.style)
            if header_level <= file_header_level:
                out_fp.write(
                    f"{'    ' * (item.level - 1)}- [{item.title}](./{item.output_filename})\n"
                )
        if extracted_document.applied_errata:
            out_fp.write("\n[Applied Errata](./AppliedErrata.md)\n")

    book_toml = output_root / "book.toml"
    if not book_toml.is_file():
        with book_toml.open("w", encoding="utf8") as fp:
            fp.write(
                f"""[book]
title = {json.dumps(extract_conf.title)}
authors = {json.dumps(extract_conf.authors)}

[output.html.print]
enable = false
"""
            )
            if extract_conf.description:
                fp.write(f"description = ")
                fp.write(extract_conf.description.replace("\n", "").replace("\r", ""))
                fp.write("\n")
