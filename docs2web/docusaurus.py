import pathlib
import typing
import json
import yaml
import io

from . import common
from .markdown import output_markdown, CommonMarkStyle


class DocusaurusStyle(CommonMarkStyle):
    def __init__(self, title: str, file_header_level: int) -> None:
        self.position = 1
        self.title = title
        self.file_header_level = file_header_level
        self.header_positions = {}
        super().__init__()

    def get_rendered_name(self, output_filename: pathlib.Path) -> typing.Optional[str]:
        if output_filename.name == "index.md":
            return str(output_filename.parent) + "/"
        return str(output_filename.with_suffix(""))

    def get_filename(
        self,
        source_directory: pathlib.Path,
        headers: typing.List[common.ExtractedHeading],
    ) -> typing.Tuple[pathlib.Path, str]:
        header_source_directory = source_directory
        for idx in range(len(headers) - 1):
            header_source_directory = (
                header_source_directory
                / super().get_filename(source_directory, headers[: idx + 1])[1]
            )

        header_source_directory.mkdir(parents=True, exist_ok=True)

        if len(headers) < self.file_header_level and any(
            common.HEADER_LEVEL_LOOKUP.get(subheading.block.style, 6)
            <= self.file_header_level
            for subheading in headers[-1].subheadings
        ):
            header_source_directory = (
                header_source_directory
                / super().get_filename(source_directory, headers)[1]
            )
            header_source_directory.mkdir(parents=True, exist_ok=True)

            with common.HashedTextFile(
                header_source_directory / "_category_.json"
            ) as fp:
                json.dump(
                    {
                        "label": headers[-1].title,
                        "position": self.position,
                        "collapsible": True,
                        "collapsed": True,
                    },
                    fp,
                    indent=2,
                )
            self.position += 1
            return header_source_directory, "index"

        self.header_positions[id(headers[-1])] = self.position
        self.position += 1
        return super().get_filename(header_source_directory, headers)

    def get_header(
        self,
        headers: typing.Union[typing.List[common.ExtractedHeading], str],
        is_appendix: bool = False,
    ) -> str:
        position = self.header_positions.get(id(headers[-1]))
        if position is None:
            position = self.position
            self.position += 1

        with io.StringIO() as fp:
            fp.write("---\n")
            yaml.dump(
                {
                    "sidebar_position": position,
                    "sidebar_label": headers if is_appendix else headers[-1].title,
                    "tags": [self.title],
                },
                fp,
            )
            fp.write("---\n\n")
            header = fp.getvalue()

        return header


def output_docusaurus(
    extract_conf: common.DocumentConfiguration,
    output_config,
    extracted_document: common.ExtractedDocument,
    out_dir: pathlib.Path,
    markdown_base: str,
    image_base: str,
    prefix: str,
    *,
    output_debug: bool = False,
    override_title: typing.Optional[str] = None,
    position: typing.Optional[int] = None
):
    source_dir = out_dir / markdown_base / prefix
    image_dir = out_dir / "static" / image_base / prefix

    image_url_base = "/" + image_base + "/"
    if prefix:
        image_url_base += prefix + "/"

    file_header_level = output_config.get("file_header_level", 2)

    output_markdown(
        extract_conf,
        DocusaurusStyle(extract_conf.title, file_header_level),
        extracted_document,
        source_dir,
        image_dir,
        image_url_base,
        output_debug=output_debug,
        file_header_level=file_header_level,
    )

    if prefix:
        with common.HashedTextFile(source_dir / "_category_.json") as fp:
            category = {
                "label": override_title or extract_conf.title,
                "collapsible": True,
                "collapsed": True,
            }
            if position is not None:
                category["position"] = position
            json.dump(
                category,
                fp,
                indent=2,
            )
