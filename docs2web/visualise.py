import typing
import pathlib

from . import common


def raw_block_to_html(
    base_path: pathlib.Path,
    fp: typing.TextIO,
    document: common.RawDocument,
    page: common.RawPage,
    page_index: int,
    page_top: int,
    page_width: int,
    page_height: int,
    page_zoom: float,
    raw_block: common.RawBlock,
    draw_images: bool,
    extra_classes: str = "",
):
    block_style = ""
    if raw_block.bbox:
        if document.left_to_right_coords:
            left = raw_block.bbox.left
        else:
            left = page_width - raw_block.bbox.left

        if document.top_to_bottom_coords:
            top = raw_block.bbox.top
        else:
            top = page_height - raw_block.bbox.top

        left = int(left * page_zoom)
        top = int(top * page_zoom)
        width = int((raw_block.bbox.right - raw_block.bbox.left) * page_zoom)
        height = int((raw_block.bbox.top - raw_block.bbox.bottom) * page_zoom)

        block_style += f"position: absolute; left: {left}px; top: {top}px; width: {width}px; height: {height}px"

    if raw_block.block_type == common.RAW_TEXT_BLOCK:
        fp.write(
            f"    <div class='text_block {extra_classes}' style='{block_style}'>\n"
        )
        for text_segment in raw_block.block.segments:
            segment_style = f"font-size: {text_segment.font_size}pt;"
            font_style = ""

            if text_segment.bold:
                segment_style += " font-weight: bold;"
                font_style += "bold"

            if text_segment.italic:
                if font_style:
                    font_style += ", "
                font_style += "italic"
                segment_style += " font-style: italic;"

            if text_segment.underline:
                if font_style:
                    font_style += ", "
                font_style += "underline"

            if text_segment.strikethrough:
                if font_style:
                    font_style += ", "
                font_style += "strikethrough"

            update_args = [
                str(page.page_number),
                f"'{text_segment.font_name}'",
                str(text_segment.font_size),
                f"'{font_style}'",
            ]
            if raw_block.bbox:
                update_args += [
                    str(raw_block.bbox.left),
                    str(raw_block.bbox.top),
                    str(raw_block.bbox.right),
                    str(raw_block.bbox.bottom),
                ]
            else:
                update_args += ["'--'"] * 4
            update_args += [
                str(text_segment.stroke_colour.red),
                str(text_segment.stroke_colour.green),
                str(text_segment.stroke_colour.blue),
                str(text_segment.fill_colour.red),
                str(text_segment.fill_colour.green),
                str(text_segment.fill_colour.blue),
            ]

            segment_text = text_segment.text
            segment_text = segment_text.replace("\n", "<br>")
            fp.write(
                f"      <span class='text_segment' onmouseover=\"hoverTextBlock({', '.join(update_args)})\" style='{segment_style}'>{segment_text}</span>\n"
            )
        fp.write("    </div>\n")
    elif raw_block.block_type == common.RAW_TABLE_BLOCK:
        fp.write(f"    <div class='table_block' style='{block_style}'>\n")
        fp.write("      <table style='border-style: solid; border-width: 1px'>\n")
        for row in raw_block.block.rows:
            fp.write("      <tr>\n")
            for cell in row.cells:
                fp.write("        <td>\n")
                for cell_block in cell.contents:
                    raw_block_to_html(
                        base_path,
                        fp,
                        document,
                        page,
                        page_index,
                        page_top,
                        page_width,
                        page_height,
                        page_zoom,
                        cell_block,
                        draw_images,
                    )
                fp.write("        </td>\n")
            fp.write("      </tr>\n")
        fp.write("      </table>\n")
        fp.write("    </div>\n")
    elif raw_block.block_type == common.RAW_IMAGE_BLOCK and draw_images:
        image_block: common.RawImage = raw_block.block
        image_path: pathlib.Path = base_path / image_block.file
        print(image_block)
        if not image_path.exists():
            print(f"Writing {image_path}")
            with image_path.open("wb") as image_fp:
                image_fp.write(image_block.read_data())

        fp.write(
            f"    <img class='image_block' src='{image_path}' style='{block_style}'/>"
        )


def visualise_as_html(
    title: str,
    extract_conf: common.DocumentConfiguration,
    document: common.RawDocument,
    out_dir: pathlib.Path,
    *,
    page_zoom: float = 1.0,
    draw_layouts: bool = True,
    draw_images: bool = False,
):
    page_top = 0

    out_dir.mkdir(parents=True, exist_ok=True)
    output_file = out_dir / "index.html"

    layout_defs = common.build_layouts(extract_conf.input_config.get("layouts", []))
    layouts: typing.Dict[int, common.LayoutDefinition] = {}
    for definition in layout_defs:
        definition.pages.apply_mappings(layouts, definition, len(document.pages))

    with open(output_file, "w", encoding="utf8") as fp:
        fp.write(
            f"""<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>{title}</title>
  <style>
    body {{
        padding: 0px;
        margin: 0px;
    }}
    div.infoBox {{
        position: fixed;
        top: 300px;
        right: 25px;
        width: 400px;
        border-style: solid;
        z-index: 10;
        background-color: lightgray;
    }}
    div.page {{
        border-style: solid;
        border-width: 1px;
        padding: 0px;
        margin: 20px;
        position: relative;
        display: inline-block;
    }}
    div.layout_section {{
        border-style: dashed;
        border-width: 1px;
        border-color: green;
        padding: 0px;
        margin: 0px;
        z-index: -5;
    }}
    div.layout_info {{
        width: 400px;
        border-style: solid;
        background-color: lightgray;
    }}
    div.text_block {{
        text-overflow: ellipsis;
        white-space: nowrap;
    }}
    div.text_block:hover {{
        text-overflow: visible;
        border-style: solid;
        border-width: 1px;
    }}
    span.text_segment:hover {{
        background-color: black;
        color: white;
    }}
    div.infoContainer {{
        display: inline-block;
    }}
    .dropped {{
        color: red;
    }}
  </style>
  <script>
    function hoverTextBlock(pageNumber, fontName, fontSize, fontStyle, left, top, right, bottom, stroke_red, stroke_green, stroke_blue, fill_red, fill_green, fill_blue) {{
      document.getElementById('infoBoxPageNumber').innerText = pageNumber
      document.getElementById('infoBoxFontName').innerText = fontName
      document.getElementById('infoBoxFontSize').innerText = fontSize
      document.getElementById('infoBoxFontStyle').innerText = fontStyle
      document.getElementById('infoBoxFontBBox').innerText = "Left: " + left + ", Top: " + top + ", Right: " + right + ", Bottom: " + bottom
      document.getElementById('infoBoxStrokeColour').innerText = "Red: " + stroke_red + ", Green: " + stroke_green + ", Blue: " + stroke_blue
      document.getElementById('infoBoxFillColour').innerText = "Red: " + fill_red + ", Green: " + fill_green + ", Blue: " + fill_blue
    }}
    function hoverLayout(elementId, highlight) {{
        var element = document.getElementById(elementId)
        if (highlight) {{
            element.style.borderWidth = "2px"
            element.style.borderStyle = "solid"
            element.style.borderColor = "red"
        }} else {{
            element.style.borderWidth = "1px"
            element.style.borderStyle = "dashed"
            element.style.borderColor = "green"
        }}
    }}
  </script>
</head>
<body>
  <div id='infoBox' class='infoBox'>
    <div><span style="font-weight: bold;">Page Number:</span> <span id="infoBoxPageNumber">--</span></div>
    <div><span style="font-weight: bold;">Font Name:</span> <span id="infoBoxFontName">--</span></div>
    <div><span style="font-weight: bold;">Font Size:</span> <span id="infoBoxFontSize">--</span></div>
    <div><span style="font-weight: bold;">Font Style:</span> <span id="infoBoxFontStyle">--</span></div>
    <div><span style="font-weight: bold;">Stroke Colour:</span> <span id="infoBoxStrokeColour">Red: --, Green: --, Blue: --</span></div>
    <div><span style="font-weight: bold;">Fill Colour:</span> <span id="infoBoxFillColour">Red: --, Green: --, Blue: --</span></div>
    <div><span style="font-weight: bold;">Block BBox:</span> <span id="infoBoxFontBBox">Left: -- Top: -- Right: -- Bottom: --</span></div>
  </div>
"""
        )
        page_index = 1
        for page in document.pages:
            page_height = page.height or 1000
            page_width = page.width or 1000

            fp.write(
                f" <div id='pageContainer_{page.page_number}' class='pageContainer'>\n"
            )

            if page.height and page.width:
                fp.write(
                    f"  <div id='page_{page.page_number}' class='page' style='height: {int(page_height * page_zoom)}px; width: {int(page_width * page_zoom)}px'>\n"
                )
            else:
                fp.write(f"  <div id='page_{page.page_number}' class='page'>\n")
            for section in page.sections:
                for raw_block in section.blocks:
                    raw_block_to_html(
                        out_dir,
                        fp,
                        document,
                        page,
                        page_index,
                        page_top,
                        page_width,
                        page_height,
                        page_zoom,
                        raw_block,
                        draw_images,
                    )

            for raw_block in page.dropped:
                raw_block_to_html(
                    out_dir,
                    fp,
                    document,
                    page,
                    page_index,
                    page_top,
                    page_width,
                    page_height,
                    page_zoom,
                    raw_block,
                    draw_images,
                    "dropped",
                )

            if draw_layouts:
                layout = layouts.get(page.page_number)
                if layout:
                    for section_idx, section in enumerate(layout.sections + [layout]):
                        x_bounds = [
                            max(0, section.x_bounds[0], layout.x_bounds[0]),
                            min(page_width, section.x_bounds[1], layout.x_bounds[1]),
                        ]
                        y_bounds = [
                            max(0, section.y_bounds[0], layout.y_bounds[0]),
                            min(page_height, section.y_bounds[1], layout.y_bounds[1]),
                        ]

                        if document.left_to_right_coords:
                            section_left = int(x_bounds[0] * page_zoom)
                        else:
                            section_left = int((page_width - x_bounds[1]) * page_zoom)
                        section_width = int((x_bounds[1] - x_bounds[0]) * page_zoom)

                        if document.top_to_bottom_coords:
                            section_top = int(y_bounds[0] * page_zoom)
                        else:
                            section_top = int((page_height - y_bounds[1]) * page_zoom)
                        section_height = int((y_bounds[1] - y_bounds[0]) * page_zoom)

                        fp.write(
                            f"    <div id='section_{page.page_number}_{section_idx}' class='layout_section' style='position: absolute; left: {section_left}px; top: {section_top}px; width: {section_width}px; height: {section_height}px' ></div>\n"
                        )

            fp.write("  </div>\n")

            if draw_layouts and layout:
                fp.write("  <div class='infoContainer'>\n")
                fp.write(
                    f"  <div id='info_box_page{page_index}' class='info_box_page'>"
                )
                fp.write(
                    f"    <div class='layout_info' onmouseover=\"hoverLayout('section_{page.page_number}_{len(layout.sections)}', true)\" onmouseout=\"hoverLayout('section_{page.page_number}_{len(layout.sections)}', false)\">"
                )
                fp.write(
                    f"      <div><span style='font-weight: bold;'>Layout Notes:</span> {layout.notes}</div>"
                )
                fp.write(
                    f"      <div><span style='font-weight: bold;'>Layout Bounds:</span> X: {layout.x_bounds} Y: {layout.y_bounds}</div>"
                )
                fp.write("    </div>")
                fp.write("    <div>")
                for section_idx, section in enumerate(layout.sections):
                    fp.write(
                        f"      <div class='layout_info' onmouseover=\"hoverLayout('section_{page.page_number}_{section_idx}', true)\" onmouseout=\"hoverLayout('section_{page.page_number}_{section_idx}', false)\">"
                    )
                    fp.write(
                        f"        <div><span style='font-weight: bold;'>Section Bounds:</span> X: {section.x_bounds} Y: {section.y_bounds}</div>"
                    )
                    fp.write(
                        f"        <div><span style='font-weight: bold;'>Section Handling:</span> {section.handling}</div>"
                    )
                    fp.write(
                        f"        <div><span style='font-weight: bold;'>Section Tag:</span> {section.tag}</div>"
                    )
                    fp.write("      </div>")
                fp.write("    </div>")
                fp.write("  </div>")
                fp.write("  </div>\n")
            fp.write(" </div>")

            page_top += page_height
            page_index += 1
        fp.write("</body></html>\n")
