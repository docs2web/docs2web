import typing

from lxml import html

from .. import common


def parse_title(title_value: str) -> typing.Dict[str, str]:
    parsed = {}
    for metadata in title_value.split(";"):
        key, value = metadata.strip().split(" ", 1)
        parsed[key] = value
    return parsed


def extract_raw_document(
    extract_conf: common.DocumentConfiguration, **kwargs
) -> common.RawDocument:
    pages = []
    hocr = html.parse(str(extract_conf.file))

    use_lines = True

    for page in hocr.xpath("body/div"):
        page_meta = parse_title(page.attrib["title"])
        dpi_x, dpi_y = page_meta.get("scan_res", "150 150").split(" ")
        dpi_x = int(dpi_x)
        dpi_y = int(dpi_y)

        blocks: typing.List[common.RawBlock] = []
        for carea in page:
            for paragraph in carea:
                for line in paragraph:
                    line_meta = parse_title(line.attrib["title"])
                    font_size = 11
                    if "x_fsize" in line_meta:
                        font_size = int(float(line_meta["x_fsize"]))
                    elif "x_size" in line_meta:
                        font_size = int(float(line_meta["x_size"]) / dpi_y * 72.0)

                    words = []
                    for word in line:
                        if word.text and word.text.strip():
                            if use_lines:
                                words.append(word.text)
                            else:
                                bbox = None

                                word_meta = parse_title(word.attrib["title"])
                                bbox_str = word_meta.get("bbox")

                                if bbox_str:
                                    left, top, right, bottom = bbox_str.split(" ")
                                    bbox = common.BoundingBox(
                                        int(left), int(top), int(right), int(bottom)
                                    )

                                blocks.append(
                                    common.RawBlock(
                                        block_type=common.RAW_TEXT_BLOCK,
                                        block=common.RawTextBlock(
                                            segments=[
                                                common.RawTextSegment(
                                                    word.text, "Noname", font_size
                                                )
                                            ]
                                        ),
                                        bbox=bbox,
                                    )
                                )
                    if use_lines and words:
                        bbox = None

                        bbox_str = line_meta.get("bbox")

                        if bbox_str:
                            left, top, right, bottom = bbox_str.split(" ")
                            bbox = common.BoundingBox(
                                int(left), int(top), int(right), int(bottom)
                            )

                        blocks.append(
                            common.RawBlock(
                                block_type=common.RAW_TEXT_BLOCK,
                                block=common.RawTextBlock(
                                    segments=[
                                        common.RawTextSegment(
                                            " ".join(words), "Noname", font_size
                                        )
                                    ]
                                ),
                                bbox=bbox,
                            )
                        )

        page_width = None
        page_height = None

        bbox_str = page_meta.get("bbox")
        if bbox_str:
            _, _, right, bottom = bbox_str.split(" ")
            page_width = int(right)
            page_height = int(bottom)

        pages.append(
            common.RawPage(
                len(pages) + 1,
                sections=[common.RawSection(blocks=blocks)],
                width=page_width,
                height=page_height,
            )
        )

    return common.RawDocument(pages=pages)
