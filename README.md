# Docs 2 Web

This project takes PDFs, DOCX and hOCR files and converts them to web friendly
documentation. Current supported outputs are mdbook and Docusaurus.

In order to successfully run the conversion, a configuration file must be
created to instruct the tool how to interpret the content of the pages.

Sample configurations may be found [here](https://gitlab.com/docs2web/doc-configs).

**Important Note:** Please do not use this tool to make copyrighted PDFs that
you do not have the publishing rights publicably available on the internet.
This tool is intended for personal use/reference only. Thank you :)

## Building, Installing, Requirements

Right now installation involves creating a new Python 3.9 venv, and pip
installing the project.

The project does have an external dependency on ImageMagick via the
[wand library](https://docs.wand-py.org/). Please see their documentation on
installing the prereqs.

## Creating a new configuration for a PDF

First copy an existing configuration file as follows:
```
{
    "title": "",
    "authors": [],
    "input_config": {
        "table_handling": {},
        "font_mapping": {
        },
        "layouts": [
        ]
    },
    "extractors": [
    ]
}
```

Fill in the title of the PDF, and a list of authors.

Next step is to dump some statistics about the PDF you will be
converting.

    python -m docs2web dump-stats --ignore-font-size [your_new_conf.json] [document_to_convert.pdf]

This will list all the font names and how many pages your PDF has. The font
names can be added to font mapping section. Each key is a regex that must have
exactly 1 group defined. Any text outside of the group is dropped from the
font name as seen by `docs2web`. For example:

```
"font_mapping": {
    ".*\\+(CaxtonStd)-Book": {
    },
    ".*\\+(PrioriSerifOT)-Bold": {
        "bold": true
    },
    ".*\\+(CaxtonStd)-Bold": {
        "bold": true
    }
}
```

This example maps 3 fonts, `CaxtonStd-Book` gets mapped to `CaxtonStd` without
bold or italics, while `CaxtonStd-Bold` also gets mapped to the same font name
`CaxtonStd` but text with this font is marked as bold.

Next you will want to define a basic layout and extractor, see the example below:

**Example Layout Entry**
```
"layouts": [
    {
        "pages": [
            "2-8"
        ],
        "x_bounds": [
            90.0,
            550.0
        ],
        "y_bounds": [
            20.0,
            430.0
        ]
    }
]
```

**Example extractors entry:**
```
"extractors": [
    {
        "pages": [
            "2-8"
        ],
        "styles": [
            {
                "style": "paragraph"
            }
        ]
    }
]
```

A few things to note about the above example:
 * Only pages 2-8 are being mapped in both sections. This means pages outside
 this range will be skipped.
 * All text will be mapped to the "paragraph style"
 * `x-bounds` and `y-bounds` define where the content of each page is. Any
 content outside these bounds will be dropped
 * PDF coordinates are left to right, but bottom to top. So `0,0` is the
 bottom right of the page.

The next step is to visualise the document:

    python -m docs2web visualise --watch --page-zoom 1.5 [your_new_conf.json] [document_to_convert.pdf] [visualisation_output_dir]

This command creates an interactive HTML representation of the PDF's text. The
`--page-zoom` parameter informs the visualisation to add a 1.5x multiplier to
the coordinates of any text. The `--watch` parameter tells `docs2web` to keep
an eye on your document and configuration files, if either changes, it
re-creates the visualisation.

Your default web browser will be launched once the visualisation is ready. On
large PDFs, this may take a few minutes, but the configuration reload is very
fast after.

From the visualisation page you will be able to see your page boundaries and
section boundaries. Hovering over text will tell you its font name, coordinates,
size, colour and anything else that could be useful to targeting it in the
`extractors` section of your config file.

Now you may edit the configuration file and refresh the visualisation to see
your changes. Once you are happy with the results, you can try rendering the
PDF as a mdbook (ensure you download the mdbook binaries for your platform
to preview and build the final output!). To output mdbook, simply run:

    python -m docs2web mdbook --watch [your_new_conf.json] [document_to_convert.pdf] [visualisation_output_dir]

Note the `--watch` parameter is once again available, so as you make changes
it will output the new Markdown files for mdbook. With `mdbook --serve`, it
will watch the directory for changes and re-create the mdbook for you.

You can also output docusaurus, see `python -m docs2web --help` and
`python -m docs2web docusaurus --help` for more information.

Head over to [sample configurations](https://gitlab.com/docs2web/doc-configs)
for more examples of building the configuration files.
